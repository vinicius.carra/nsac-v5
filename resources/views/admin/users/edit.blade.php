@extends('adminlte::page')

@section('title', 'Usuário')

@section('content_header')
    <h1>Usuários <small>Edição</small></h1>
@stop

@section('content')


    <div class="box box-primary">

        <div class="box-header with-border">
        <h3 class="box-title"><i class='fa fa-user-plus'></i> Editar Usuário: {{$user->name}}</h3>
        </div>
<div class="box-body">
        {{ Form::model($user, array('route' => array('admin.users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

        <div class="form-group">
            {{ Form::label('name', 'Nome') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>
        <div class='form-group'>
        <h5><b>Atribuir tipo</b></h5>

            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                {{-- {{ Form::label($role->name, ucfirst($role->name)) }}<br> --}}
                {{ ucfirst($role->name) }} <br>
            @endforeach
        </div>

        <div class="form-group">
            {{ Form::label('password', 'Senha') }}<br>
            {{ Form::password('password', array('class' => 'form-control')) }}

        </div>

        <div class="form-group">
            {{ Form::label('password', 'Confirmar Senha') }}<br>
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

        </div>

        {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
</div>
    </div>

@endsection
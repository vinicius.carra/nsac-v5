<div class="form-group">
    {{ Form::label('descricao', 'Nome do Item') }}
    {{ Form::text('descricao', null, array('class' => 'form-control','required'=>'required')) }}
</div>

<div class="checkbox">
    <label>
        {{ Form::checkbox('tem_texto') }}
        Permitir digitação de texto
    </label>
</div>
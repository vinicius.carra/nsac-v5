@extends('adminlte::page')

@section('title', 'Itens da Ficha de Avaliação')

@section('content_header')
    <h1>{{--<i class="fa fa-key"></i>--}} Itens do Grupo de Ficha de Avaliação <small>Adicionando Item</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header">
        <h3 class="box-title">Novo Item</br><small>Grupo: {{$grupo->descricao}}</small></h3>
        </div>

        <div class="box-body">
            {{ Form::open(array('url' => 'fichaitens/store')) }}

            @include('professor.ficha-avaliacao.itens._form')

            {{ Form::hidden('codigo_grupo',$grupo->codigo) }}

        </div>
        <div class="box-footer">

            {{ Form::submit('Criar Item', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}

        </div>

    </div>

@endsection
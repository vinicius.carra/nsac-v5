@extends('adminlte::page')

@section('title', 'Perfis')

@section('content_header')
    <h1>{{--<i class="fa fa-key"></i>--}} Grupos de Ficha de Avaliação <small>Lista dos Grupos de Ficha de Avaliação Cadastrados</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Nome do Grupo</th>
                    <th>Operação</th>
                </tr>
                </thead>

                <tbody>
                @forelse ($fichasgrupo as $fgrupo)
                    <tr>
                        <td>{{ $fgrupo->descricao }}</td>
                        <td>
                            <a href="{{ Route('fichagrupos.edit', $fgrupo->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Editar</a>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['fichagrupos.destroy', $fgrupo->id] ]) !!}
                            @if($fgrupo->ativo==TRUE)
                                {!! Form::submit('Desativar', ['class' => 'btn btn-danger']) !!}
                            @endif
                            @if($fgrupo->ativo==FALSE)
                                {!! Form::submit('Ativar', ['class' => 'btn btn-danger']) !!}
                            @endif
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @empty
                    <td colspan="3">Nenhum Grupo cadastrado até o momento</td>
                @endforelse
                </tbody>

            </table>
        </div>
        <div class="box-footer with-border">
            <a href="{{ Route('fichagrupos.create') }}" class="btn btn-success">Novo Grupo</a>
        </div>
    </div>

@endsection
@extends('adminlte::page')

@section('title', 'Professores')

@section('content_header')
    <h1>Professores <small>Visualizar Professor</small></h1>
@stop

@section('content')
    <div class="container">

        <h1>{{ $professor->Nome }}</h1>
        <hr>
        <p class="lead">{{ $professor->email }} </p>
        @foreach($atribuicoes->all() as $atrib)
        <p> {{ $atrib->data_inicial }} Até {{ $atrib->data_final }}</p>
        @endforeach
        <hr>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.professores.destroy', $professor->codigo] ]) !!}
        <a href="{{ url()->previous() }}" class="btn btn-primary">Voltar</a>
        @can('Edit Professor')
            <a href="{{ route('admin.professores.edit', $professor->codigo) }}" class="btn btn-info" role="button">Editar</a>
        @endcan
        @can('Delete Professor')
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        @endcan
        {!! Form::close() !!}

    </div>

@endsection
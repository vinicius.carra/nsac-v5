@extends('adminlte::page')

@section('title', 'Professores')

@section('content_header')
    <h1>Professores <small>Lista das Professores Cadastrados</small></h1>
@stop

@section('content')

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Lista Geral
                        </h3><a href="{{-- route('admin.professores.create') --}}" class="btn btn-primary pull-right">Novo Professor</a>
                    </div>
                    <div class="box-header with-border">Página {{ $professores->currentPage() }} de {{ $professores->lastPage() }}</div>

                    <div class="box-body">
                        <table class="table table-bordered table-striped">

                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>CPF</th>
                                <th>Operações</th>
                            </tr>
                            </thead>

                            <tbody>

                        @foreach ($professores as $professor)
                            <tr>
                            <td>{{ $professor->nome }}</td>
                            <td>{{ $professor->email }}</td>
                            <td>{{ $professor->cpf_formatado }}</td>

                            <td>
                                <a href="{{ route('admin.professores.show', $professor->codigo) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Ver</a>
                                <a href="{{ route('admin.professores.edit', $professor->codigo) }}" class="btn btn-info" role="button">Editar</a>
                                <a href="{{ route('admin.professores.destroy', $professor->codigo) }}" class="btn btn-danger" role="button">Apagar</a>

                            </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                    </div>

                <div class="box-footer text-center">
                    {!! $professores->links() !!}
                </div>

            </div>

@endsection
@extends('adminlte::page')

@section('title', 'Permissões de Usuário')

@section('content_header')
    <h1><i class="fa fa-key"></i> Permissões de Usuário <small>Lista das Permissões Cadastradas</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Permissões</th>
                    <th>Operações</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        <td>
                            <a href="{{ Route('admin.permissions.edit', $permission->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Editar</a>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.permissions.destroy', $permission->id] ]) !!}
                            {!! Form::submit('Apagar', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer with-border">
            <a href="{{ Route('admin.permissions.create') }}" class="btn btn-success">Nova Permissão</a>
        </div>
    </div>

@endsection
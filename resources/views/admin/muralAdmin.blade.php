@extends('adminlte::page')

@section('title', 'Mural administrador')

@section('content_header')
    <h1>NSac
        <small>{{date('d/m/Y')}}</small>
    </h1>
@stop


@section('content')
    @if(empty($sala))
        <div class="form-group">
            <label>Informática</label>
            {{ Form::open(array('route' => 'admin.mural.selecionar')) }}
            <select class="form-control" name="turma">
                @foreach($info as $turma)
                    <option value="{{ $turma->codigo }}">{{ $turma->nomenclatura }}</option>
                @endforeach
            </select>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
        <hr>
        <div class="form-group">
            <label>Eletrônica</label>
            {{ Form::open(array('route' => 'admin.mural.selecionar')) }}
            <select class="form-control" name="turma">
                @foreach($eletro as $turma)
                    <option value="{{ $turma->codigo }}">{{ $turma->nomenclatura }}</option>
                @endforeach
            </select>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
        <hr>
        <div class="form-group">
            <label>Mecânica</label>
            {{ Form::open(array('route' => 'admin.mural.selecionar')) }}
            <select class="form-control" name="turma">
                @foreach($meca as $turma)
                    <option value="{{ $turma->codigo }}">{{ $turma->nomenclatura }}</option>
                @endforeach
            </select>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    @else

        @php
            $disciplinas = App\Models\Disciplina::get()->where('turma', '=', $sala->codigo);
        @endphp

        <div class="row-content">
            <div class="row panel-body">
                <div class="col-sm-12">
                    <table id="example2"
                           class="table table-bordered table-hover dataTable"
                           role="grid" aria-describedby="example2_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">
                                <center>Data</center>
                            </th>
                            <th class="sorting" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-label="Browser: activate to sort column ascending">
                                1º Horário <br>(Das 7h10 às 8h40)
                            </th>
                            <th class="sorting" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-label="Platform(s): activate to sort column ascending">
                                2º Horário <br>(Das 9h00 às 10h30)
                            </th>
                            <th class="sorting" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-label="Engine version: activate to sort column ascending">
                                3º Horário <br>(Das 10h50 às 12h20)
                            </th>
                            <th class="sorting" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-label="CSS grade: activate to sort column ascending">
                                4º Horário <br>(Das 14h00 às 15h30)
                            </th>
                            <th class="sorting_asc" tabindex="0"
                                aria-controls="example2" rowspan="1" colspan="1"
                                aria-sort="ascending"
                                aria-label="Rendering engine: activate to sort column descending">
                                5º Horário <br>(Das 15h50 às 17h20)
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr role="row">
                            <td>Segunda</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Segunda')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td>-</td>
                                @else
                                    <td><center>{{ $prova->getDisciplina->abreviacao }}<br><small>({{ $prova->professor }})</small></center></td>
                                @endif
                            @endforeach
                        </tr>
                        <tr role="row">
                            <td>Terça</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Terça')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td></td>
                                @else
                                    <td><center>{{ $prova->getDisciplina->abreviacao }}<br><small>({{ $prova->professor }})</small></center></td>
                                @endif
                            @endforeach
                        </tr>
                        <tr role="row">
                            <td>Quarta</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Quarta')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td>-</td>
                                @else
                                    <td><center>{{ $prova->getDisciplina->abreviacao }}<br><small>({{ $prova->professor }})</small></center></td>
                                @endif
                            @endforeach
                        </tr>
                        <tr role="row">
                            <td>Quinta</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Quinta')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td></td>
                                @else
                                    <td><center>{{ $prova->getDisciplina->abreviacao }}<br><small>({{ $prova->professor }})</small></center></td>
                                @endif
                            @endforeach
                        </tr>
                        <tr role="row">
                            <td>Sexta</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Sexta')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td></td>
                                @else
                                    <td>
                                    {{ Form::open(array('route' => 'admin.mural.selecionar')) }}
                                        <select name="disciplina">
                                            <option value="-1">Sem prova</option>
                                            @foreach($disciplinas as $d)
                                                <option value="{{ $d->codigo }}">{{ $d->abreviacao }}</option>
                                            @endforeach    
                                        </select>
                                        <input type="hidden" name="horario" value="{{ $prova->horario }}">
                                    </form>
                                    </td>
                                @endif
                            @endforeach
                        </tr>
                        <tr role="row">
                            <td>Sábado</td>
                            @php
                                $query = App\Models\Prova::orderBy('horario')
                                ->where('turma', '=', $sala->codigo)
                                ->where('dia', '=', 'Sábado')
                                ->where('bimestre', '=', 1)
                                ->get();
                            @endphp
                            @foreach($query as $prova )
                                @if($prova->professor == '-')
                                    <td></td>
                                @else
                                    <td><center>{{ $prova->getDisciplina->abreviacao }}<br><small>({{ $prova->professor }})</small></center></td>
                                @endif
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif







@stop
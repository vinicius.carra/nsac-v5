@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Verificar código de declaração</p>
        {{ Form::open(array('route' => 'declaracao.verificar', 'id' => 'declaracao')) }}

            <div class="form-group">
                <input type="text" name="codigo" class="form-control"
                       placeholder="Código de declaração" required>
            </div>
            <div class="row">
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Verificar</button>
                </div>
            </div>
        </form>
        <div class="auth-links">
            @if (config('adminlte.register_url', 'register'))
                <a href="{{ url(config('adminlte.register_url', 'register')) }}"
                   class="text-center"
                >{{ trans('adminlte::adminlte.register_a_new_membership') }}</a>
            @endif
        </div>
        @if(!empty($declaracao))
            <hr>
            <h4 class="text-center">Resultado</h4>
            <dl>
                <dt>Informações do aluno</dt>
                <dd>{{ $declaracao->dado->nome }} - {{ $declaracao->dado->matricula }}</dd>
                <dt>Código da declaração</dt>
                <dd>{{ $declaracao->codigo }}</dd>
                <dt>Data de emissão/expiração</dt>
                <dd>{{ date('d/m/Y', strtotime($declaracao->data_emissao)) }} - {{ date('d/m/Y', strtotime($declaracao->data_expira)) }}</dd>
                <dt>Status</dt>
                <dd>
                    @if(strtotime(date('Y-m-d h:m:s')) > strtotime($declaracao->data_expira))
                        <p class="text-red"><b>Expirou</b></p>
                    @else
                        <p class="text-green"><b>Válido</b></p>
                    @endif
                </dd>
            </dl>
            <hr>
            {{ Form::open(array('route' => 'declaracao.verificar.PDF', 'id' => 'pdf')) }}
            <input type="hidden" name="i_aluno" value="{{ $declaracao->dado->nome }} - {{ $declaracao->dado->matricula }}">
            <input type="hidden" name="codigo" value="{{ $declaracao->codigo }}">
            <input type="hidden" name="datas" value="{{ date('d/m/Y', strtotime($declaracao->data_emissao)) }} - {{ date('d/m/Y', strtotime($declaracao->data_expira)) }}">
            <input type="hidden" name="status" value="{{ (strtotime(date('Y-m-d h:m:s')) > strtotime($declaracao->data_expira)) ? 'Expirou' : 'Válido' }}">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Gerar PDF</button>
            </form>
        @endif
    </div>
    <!-- /.login-box-body -->
    <div class="box-footer" style="text-align: center;">
        <small>Colégio Técnico Industrial "Prof. Isaac Portal Roldán" UNESP Bauru
            <br>
            Copyright © {{date('Y')}} NSac Online

        </small>        </div>
</div><!-- /.login-box -->
@stop

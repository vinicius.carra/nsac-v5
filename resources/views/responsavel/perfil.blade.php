@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <h1>Perfil
        <small>Informações do responsável</small>
    </h1>
@stop

@section('content')
    
    <div class="row">
        <div class="col-md-8">
            <!-- Informações do perfil -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center">{{ $perfil->nome }}</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>E-mail</b> <span class="pull-right">{{ $perfil->email }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Profissão</b> <span class="pull-right">{{ $perfil->profissao }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Empresa</b> <span class="pull-right">{{ $perfil->empresa }}</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Telefones -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center">Telefones</h3>
                    <ul class="list-group list-group-unbordered">
                        @foreach($perfil->telefones as $telefone)
                            <li class="list-group-item">
                                <form action="{{route('responsavel.telefone.excluir', $telefone->id)}}"
                                      id="{{$telefone->id}}"></form>
                                <span>
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    &nbsp;&nbsp; {{ $telefone->telefone }}
                                </span>
                                <a href="#" class="pull-right" onclick="$('#{{$telefone->id}}').submit()">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <form action="{{route('responsavel.telefone.adicionar')}}">
                        <div class="row">
                            <div class="col-md-9">
                                <input required type="text" class="form-control" name="telefone" id="telefone">
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-block btn-success">
                                    <i class="fa fa-plus"
                                       aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Painel alunos -->
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center">Alunos</h3>
                    <ul class="list-group list-group-unbordered">
                        @foreach($alunos as $aluno)
                            <li class="list-group-item">
                                <a href="{{route('responsavel.alunos', $aluno->matricula)}}">
                                    <span>{{ $aluno->nome }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#telefone').mask('(00) 0000-00000');
        });
    </script>
@stop
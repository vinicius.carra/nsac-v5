@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <h1>Perfil
        <small>Informações do aluno</small>
    </h1>
@stop

@section('content')
    
    <div class="row">
        <div class="col-md-4">
            <!-- Informações do perfil -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="{{ $avatar }}" alt="User profile picture">
                    <h3 class="profile-username text-center">{{ $perfil->nome }}</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Apelido</b> <span
                                    class="pull-right">{{ ($perfil->apelido == '') ? '--' : $perfil->apelido }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Data de nascimento</b> <span
                                    class="pull-right">{{ date('d/m/Y', strtotime($perfil->data_de_nascimento)) }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>RG</b> <span class="pull-right">{{ $perfil->rg }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>CPF</b> <span class="pull-right">{{ $perfil->cpf }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>E-mail</b> <span class="pull-right">{{ $perfil->email }}</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Telefones -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center">Telefones</h3>
                    <ul class="list-group list-group-unbordered">
                        @foreach($perfil->telefones as $telefone)
                            <li class="list-group-item">
                                {{ Form::open(array('route' => 'aluno.perfil.excluirtel', 'id' => $telefone->codigo)) }}
                                    <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        {{ $telefone->numero }}
                                        {{ ($telefone->descricao == '') ? '' : ' ('.$telefone->descricao.') ' }}
                                        <input type="hidden" value="{{ $telefone->codigo }}" name="codigo">
                                    </span>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- Painel endereço e responsáveis -->
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#endereco" data-toggle="tab">Endereço</a>
                    </li>
                    <li>
                        <a href="#boletim" data-toggle="tab">Boletim</a>
                    </li>
                    <li>
                        <a href="#atrasos" data-toggle="tab">Atrasos</a>
                    </li>
                    <li>
                        <a href="#saidas" data-toggle="tab">Saídas adiantadas</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="endereco">
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::open(array('route' => 'aluno.perfil.edit', 'id' => 'endereco-form')) }}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="rua">Logradouro
                                    </label>
                                    <input type="text" required class="form-control"
                                           value="{{ $perfil->getEndereco->logradouro }}" id="rua" name="rua"
                                           max-lenght="60" placeholder="Ex: Avenida Nações Unidas" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="numero">Numero
                                    </label>
                                    <input type="number" required class="form-control"
                                           value="{{ $perfil->getEndereco->numero }}" id="numero" name="numero"
                                           placeholder="Ex: 652" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="complemento">Complemento</label>
                                    <input type="text" class="form-control" name="complemento"
                                           value="{{ ($perfil->getEndereco->complemento == '') ? 'Não disponível' : $perfil->getEndereco->complemento  }}"
                                           id="complemento" max-lenght="20" placeholder="Ex: Próximo à padaria XY"
                                           disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cidade
                                    </label>
                                    <select name="cidade" required id="cidade" class="form-control" disabled>
                                        @foreach($cidades as $cidade)
                                            <option value="{{ $cidade->codigo }}" {{ ($cidade->codigo == $perfil->getEndereco->getCidade->codigo) ? 'selected' : '' }} >{{ $cidade->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bairro">Bairro
                                    </label>
                                    <input type="text" required class="form-control" name="bairro"
                                           value="{{ $perfil->getEndereco->bairro }}" id="bairro" max-lenght="20"
                                           placeholder="Ex: Araruna, etc..." disabled>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="boletim">
                        <div class="row">
                            @include('aluno.boletimtable', ['notas' => $perfil->notas])
                        </div>
                    </div>
                    <div class="tab-pane" id="atrasos">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Motivo</th>
                                <th style="width: 40%">Data</th>
                            </tr>
                            @foreach($perfil->atrasos as $atraso)
                                <tr>
                                    <td>{{ $atraso->motivo }}</td>
                                    <td>{{ date('d/m/Y', strtotime($atraso->date)) }} {{ $atraso->hora }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="saidas">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Motivo</th>
                                <th style="width: 40%">Data</th>
                            </tr>
                            @foreach($perfil->saidas as $saida)
                                <tr>
                                    <td>{{ $saida->motivo }}</td>
                                    <td>{{ date('d/m/Y', strtotime($saida->data)) }} {{ $saida->hora }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <h1>Perfil
        <small>Informações do aluno</small>
    </h1>
@stop

@section('content')
    
    <div class="row">
        <div class="col-md-4">
            <!-- Informações do perfil -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="{{ $avatar }}" alt="User profile picture">
                    <h3 class="profile-username text-center">{{ $perfil->nome }}</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Apelido</b> <span
                                    class="pull-right">{{ ($perfil->apelido == '') ? '--' : $perfil->apelido }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>Data de nascimento</b> <span
                                    class="pull-right">{{ date('d/m/Y', strtotime($perfil->data_de_nascimento)) }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>RG</b> <span class="pull-right">{{ $perfil->rg }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>CPF</b> <span class="pull-right">{{ $perfil->cpf }}</span>
                        </li>
                        <li class="list-group-item">
                            <b>E-mail</b> <span class="pull-right">{{ $perfil->email }}</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Telefones -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center">Telefones</h3>
                    <ul class="list-group list-group-unbordered">
                        @foreach($perfil->telefones as $telefone)
                            <li class="list-group-item">
                                {{ Form::open(array('route' => 'aluno.perfil.excluirtel', 'id' => $telefone->codigo)) }}
                                    <span>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        {{ $telefone->numero }}
                                        {{ ($telefone->descricao == '') ? '' : ' ('.$telefone->descricao.') ' }}
                                        <span style="cursor: pointer;" onClick="excluirTelefone({{ $telefone->codigo }})"
                                              class="pull-right">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </span>
                                        <input type="hidden" value="{{ $telefone->codigo }}" name="codigo">
                                    </span>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                    {{ Form::open(array('route' => 'aluno.perfil.adicionartel')) }}
                    <div class="row">
                        <div class="col-md-9">
                            <input required type="text" id="field_tel" class="form-control" name="num_telefone">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-block btn-success">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Painel endereço e responsáveis -->
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#endereco" data-toggle="tab">Endereço</a>
                    </li>
                    <li>
                        <a href="#responsaveis" data-toggle="tab">Responsáveis</a>
                    </li>
                    <li>
                        <a href="#atrasos" data-toggle="tab">Atrasos</a>
                    </li>
                    <li>
                        <a href="#saidas" data-toggle="tab">Saídas adiantadas</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="endereco">
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::open(array('route' => 'aluno.perfil.edit', 'id' => 'endereco-form')) }}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="rua">Logradouro
                                        <small>*</small>
                                    </label>
                                    <input type="text" required class="form-control"
                                           value="{{ $perfil->getEndereco->logradouro }}" id="rua" name="rua"
                                           max-lenght="60" placeholder="Ex: Avenida Nações Unidas" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="numero">Numero
                                        <small>*</small>
                                    </label>
                                    <input type="number" required class="form-control"
                                           value="{{ $perfil->getEndereco->numero }}" id="numero" name="numero"
                                           placeholder="Ex: 652" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="complemento">Complemento</label>
                                    <input type="text" class="form-control" name="complemento"
                                           value="{{ ($perfil->getEndereco->complemento == '') ? 'Não disponível' : $perfil->getEndereco->complemento  }}"
                                           id="complemento" max-lenght="20" placeholder="Ex: Próximo à padaria XY"
                                           disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cidade
                                        <small>*</small>
                                    </label>
                                    <select name="cidade" required id="cidade" class="form-control" disabled>
                                        @foreach($cidades as $cidade)
                                            <option value="{{ $cidade->codigo }}" {{ ($cidade->codigo == $perfil->getEndereco->getCidade->codigo) ? 'selected' : '' }} >{{ $cidade->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bairro">Bairro
                                        <small>*</small>
                                    </label>
                                    <input type="text" required class="form-control" name="bairro"
                                           value="{{ $perfil->getEndereco->bairro }}" id="bairro" max-lenght="20"
                                           placeholder="Ex: Araruna, etc..." disabled>
                                </div>
                                </form>
                            </div>
                            <div class="col-md-12"></div>
                            <div id="alterar" class="col-md-2">
                                <button type="button" id="btn-alterar" class="btn btn-block btn-primary btn-sm">
                                    Alterar
                                </button>
                            </div>
                            <div id="salvar" class="col-md-2" style="display: none;">
                                <button type="submit" id="btn-salvar" class="btn btn-block btn-success btn-sm">Salvar
                                </button>
                            </div>
                            <div id="cancelar" class="col-md-2" style="display: none;">
                                <button type="button" id="btn-cancelar" class="btn btn-block btn-danger btn-sm">
                                    Cancelar
                                </button>
                            </div>
                        </div>
                        <small>Campos com * são obrigatórios</small>
                    </div>
                    <div class="tab-pane" id="responsaveis">
                        <div class="row">
                            @foreach($perfil->responsaveis as $responsavel)
                                <div class="col-md-6">
                                    <div class="box box-widget widget-user-2">
                                        <div class="widget-user-header bg-blue">
                                            <h3 class="widget-user-username"
                                                style="margin: 0 0 0 0">{{ $responsavel->nome }}</h3>
                                            <h5 class="widget-user-desc"
                                                style="margin: 0 0 0 0">{{ $responsavel->profissao }} - {{ $responsavel->empresa }}</h5>
                                        </div>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                @foreach($responsavel->telefones as $telefone)
                                                    <li><a href="#">{{ $telefone->telefone }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane" id="atrasos">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>Motivo</th>
                                    <th style="width: 40%">Data</th>
                                </tr>
                                @foreach($perfil->atrasos as $atraso)
                                    <tr>
                                        <td>{{ $atraso->motivo }}</td>
                                        <td>{{ date('d/m/Y', strtotime($atraso->date)) }} {{ $atraso->hora }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="saidas">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Motivo</th>
                                <th style="width: 40%">Data</th>
                            </tr>
                            @foreach($perfil->saidas as $saida)
                                <tr>
                                    <td>{{ $saida->motivo }}</td>
                                    <td>{{ date('d/m/Y', strtotime($saida->data)) }} {{ $saida->hora }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4"><a href="{{ route('aluno.declaracao.PDF') }}" target="_blank"><button class="btn btn-block btn-success col-md-3">Gerar declaração</button></a></div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    <script src="{{ asset('js/aluno-perfil.js') }}"></script>
@stop
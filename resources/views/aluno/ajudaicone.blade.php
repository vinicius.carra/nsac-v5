@extends('adminlte::page')

@section('title', 'Ajuda - Ícone')

@section('content_header')
    <h1>Ajuda - Adicionar ícones</h1>
@stop

@section('content')

        <!-- Feature -->
        <div class="container-fluid">
            <div class="row">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Android</b></h3>
                </div>
                  <div class="row">
                    <div class="box-body" style="">
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">1º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Vá até o navegador* web, acesse o endereço eletrônico do NSac (www.cti3.feb.unesp/nsac/) e selecione o ícone de opções</li>
                                  </ul>
                              </div>
                              <div class="box-footer">
                                <small><i>*O Google Chrome fora usado nesse exemplo</i></small>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">2º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Selecione a opção “Adicionar à tela inicial”</li>
                                  </ul>
                                  <br><br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">3º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Coloque o nome de sua preferência e clique no botão “Adicionar”</li>
                                  </ul>
                                  <br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">4º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Pronto, o seu ícone de acesso rápido ao sistema NSac está na tela inicial do seu smartphone.</li>
                                  </ul>
                                  <br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
            <div class="row">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>iOS</b></h3>
                  </div>
                  <div class="row">
                    <div class="box-body" style="">
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">1º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Vá até o navegador* web, acesse o endereço eletrônico do NSac (www.cti3.feb.unesp/nsac/) e selecione o ícone de compartilhamento</li>
                                  </ul>
                              </div>
                              <div class="box-footer">
                                <small><i>*O Safari fora usado nesse exemplo</i></small>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">2º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Selecione o botão “Tela de início”</li>
                                  </ul>
                                  <br><br><br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">3º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Coloque o nome de sua preferência e clique no botão “Adicionar”</li>
                                  </ul>
                                  <br><br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                      <div class="col-sm-3">
                          <div class="box">
                              <div class="box-header with-border">
                                  <h3 class="box-title">4º Passo</h3>
                              </div>
                              <div class="box-body">
                                  <ul>
                                      <li>Pronto, o seu ícone de acesso rápido ao sistema NSac está na tela inicial do seu smartphone</li>
                                  </ul>
                                  <br><br><br>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /.info-box-content -->
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <div class="row">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"><b>Windows</b></h3>
                </div>
                <div class="row">
                  <div class="box-body" style="">
                    <div class="col-sm-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">1º Passo</h3>
                            </div>
                            <div class="box-body">
                                <ul>
                                    <li>Vá até o navegador* web, acesse o endereço eletrônico do NSac (www.cti3.feb.unesp/nsac/) e clique o ícone de opções.</li>
                                </ul>
                            </div>
                            <div class="box-footer">
                              <small><i>*O Google Chrome fora usado nesse exemplo</i></small>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">2º Passo</h3>
                            </div>
                            <div class="box-body">
                                <ul>
                                    <li>Clique na opção “Mais ferramentas”.</li>
                                </ul>
                                <br><br><br><br><br>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">3º Passo</h3>
                            </div>
                            <div class="box-body">
                                <ul>
                                    <li>Clique na opção “Adicionar à área de trabalho”.</li>
                                </ul>
                                <br><br><br><br>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">4º Passo</h3>
                            </div>
                            <div class="box-body">
                                <ul>
                                    <li>Coloque o nome de sua preferência e clique em adicionar</li>
                                </ul>
                                <br><br><br><br>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="row">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><b>macOS</b></h3>
              </div>
              <div class="row">
                <div class="box-body" style="">
                  <div class="col-sm-3">
                      <div class="box">
                          <div class="box-header with-border">
                              <h3 class="box-title">1º Passo</h3>
                          </div>
                          <div class="box-body">
                              <ul>
                                  <li>vá até o navegador* web, acesse o endereço eletrônico do NSac (www.cti3.feb.unesp/nsac/).</li>
                              </ul>
                          </div>
                          <div class="box-footer">
                            <small><i>*O Safari fora usado nesse exemplo</i></small>
                          </div>
                          <!-- /.box-body -->
                      </div>
                      <!-- /.info-box-content -->
                  </div>
                  <div class="col-sm-3">
                      <div class="box">
                          <div class="box-header with-border">
                              <h3 class="box-title">2º Passo</h3>
                          </div>
                          <div class="box-body">
                              <ul>
                                  <li>Clique no endereço e arraste-o até a sua área de trabalho.</li>
                              </ul>
                              <br><br><br><br>
                          </div>
                          <!-- /.box-body -->
                      </div>
                      <!-- /.info-box-content -->
                  </div>
                  <div class="col-sm-3">
                      <div class="box">
                          <div class="box-header with-border">
                              <h3 class="box-title">3º Passo</h3>
                          </div>
                          <div class="box-body">
                              <ul>
                                  <li>Solte o conteúdo na área de trabalho do seu desktop.</li>
                              </ul>
                              <br><br><br><br>
                          </div>
                          <!-- /.box-body -->
                      </div>
                      <!-- /.info-box-content -->
                  </div>
                  <div class="col-sm-3">
                      <div class="box">
                          <div class="box-header with-border">
                              <h3 class="box-title">4º Passo</h3>
                          </div>
                          <div class="box-body">
                              <ul>
                                  <li>Pronto, o seu ícone de acesso rápido ao sistema NSac está na área de trabalho de seu desktop.</li>
                              </ul>
                              <br><br><br>
                          </div>
                          <!-- /.box-body -->
                      </div>
                      <!-- /.info-box-content -->
                  </div>
              </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
        </div>

@endsection

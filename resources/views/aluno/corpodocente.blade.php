@extends('adminlte::page')

@section('title', 'Corpo Docente')

@section('content_header')
    <h1>Corpo Docente</h1>
@stop

@section('content')

        <!-- Feature -->

        <div class="container-fluid">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tbody><tr>
                    <th>Nome</th>
                    <th>Email</th>
                  </tr>
                  @foreach ($professores as $professor)
                    @if (($professor->email) != null AND
                      ($professor->situacao) == 0)
                      <tr>
                        <td>{{ $professor->nome }}</td>
                        <td>{{ $professor->email }} </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody></table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>

@endsection

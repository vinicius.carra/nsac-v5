@extends('adminlte::page')

@section('title', 'Ajuda')

@section('content_header')
    <h1>Ajuda</h1>
@stop

@section('content')

        <!-- Feature -->

        <div class="container-fluid">
            <div class="row">
                <a href="ajudaicone" target="_self" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-phone"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Ícones</span>
                                <span class="info-box-number">Adicione ícones a tela inicial do dispositivo</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>

                <a href="ajudalogin" target="_self" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-log-in"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Login</span>
                                <span class="info-box-number">Faça login no sistema NSac</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>
                <a href="" download="manual.pdf" target="_self" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-save-file"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Manual</span>
                                <span class="info-box-number">Baixe o Manual do Usuário completo do sistema NSac</small>
                            </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>
                
            </div>
        </div>

@endsection

@extends('adminlte::page')

@section('title', 'Mural')

@section('content_header')
    <h1>Mural
        <small>Informações</small>
    </h1>
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="dropdown active">
            <a class="dropdown-toggle" data-toggle="dropdown" href="geral" data-target="">
                Horário de provas <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#provas1" data-toggle="tab">1&deg; Bimestre</a></li>
                <li><a href="#provas2" data-toggle="tab">2&deg; Bimestre</a></li>
                <li><a href="#provas3" data-toggle="tab">3&deg; Bimestre</a></li>
                <li><a href="#provas4" data-toggle="tab">4&deg; Bimestre</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="mural" data-target="">
                Horário de aulas <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#info-aulas" data-toggle="tab">Informática</a></li>
                <li><a href="#eletro-aulas" data-toggle="tab">Eletrônica</a></li>
                <li><a href="#meca-aulas" data-toggle="tab">Mecânica</a></li>
            </ul>
        </li>
    </ul>
    <div id="tab-content" class="tab-content">
        <div class="tab-pane fade active in" id="provas1">
            <h2>
                1 Bimestre
            </h2>
            <div class="tab-pane fade active in" id="geral">
                <div class="list-group">
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Informática</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="/*display: none;*/">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($info as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eletrônica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($eletro as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mecânica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($meca as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 1)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade active in" id="provas2">
            <h2>
                2 Bimestre
            </h2>
            <div class="tab-pane fade active in" id="geral">
                <div class="list-group">
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Informática</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($info as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}00">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}00" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eletrônica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($eletro as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}00">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}00" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mecânica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($meca as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}00">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}00" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 2)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade active in" id="provas3">
            <h2>
                3 Bimestre
            </h2>
            <div class="tab-pane fade active in" id="geral">
                <div class="list-group">
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Informática</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($info as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}000">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}000" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova)
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid{{-- collapsed-box--}}">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eletrônica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($eletro as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}000">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}000" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mecânica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($meca as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}000">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}000" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 3)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade active in" id="provas4">
            <h2>
                4 Bimestre
            </h2>
            <div class="tab-pane fade active in" id="geral">
                <div class="list-group">
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Informática</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($info as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}001">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}001" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td>-</td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Eletrônica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($eletro as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}001">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}001" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <hr>
                    <div class="box box-default box-solid collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mecânica</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: none;">
                            <div class="list-group-item">
                                <div class="row-content">
                                    <!-- box -->
                                    @foreach($meca as $turma)
                                        <div class="box">
                                            <div class="box-header with-border panel-group " data-toggle="collapse"
                                                 href="#{{ $turma->codigo }}001">
                                                <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                        ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool"
                                                            data-widget="collapse" data-toggle="tooltip" title=""
                                                            data-original-title="Collapse">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="panel-collapse collapse " id="{{ $turma->codigo }}001" style="">
                                                <div class="row panel-body">
                                                    <div class="col-sm-12">
                                                        <table id="example2"
                                                               class="table table-bordered table-hover dataTable"
                                                               role="grid" aria-describedby="example2_info">
                                                            <thead>
                                                            <tr role="row">
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    <center>Data</center>
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Browser: activate to sort column ascending">
                                                                    1º Horário <br>(Das 7h10 às 8h40)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Platform(s): activate to sort column ascending">
                                                                    2º Horário <br>(Das 9h00 às 10h30)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="Engine version: activate to sort column ascending">
                                                                    3º Horário <br>(Das 10h50 às 12h20)
                                                                </th>
                                                                <th class="sorting" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-label="CSS grade: activate to sort column ascending">
                                                                    4º Horário <br>(Das 14h00 às 15h30)
                                                                </th>
                                                                <th class="sorting_asc" tabindex="0"
                                                                    aria-controls="example2" rowspan="1" colspan="1"
                                                                    aria-sort="ascending"
                                                                    aria-label="Rendering engine: activate to sort column descending">
                                                                    5º Horário <br>(Das 15h50 às 17h20)
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row">
                                                                <td>Segunda</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Segunda')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Terça</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Terça')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quarta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quarta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Quinta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Quinta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sexta</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sexta')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            <tr role="row">
                                                                <td>Sábado</td>
                                                                @php
                                                                    $query = App\Models\Prova::orderBy('horario')
                                                                    ->where('turma', '=', $turma->codigo)
                                                                    ->where('dia', '=', 'Sábado')
                                                                    ->where('bimestre', '=', 4)
                                                                    ->get();
                                                                @endphp
                                                                @foreach($query as $prova )
                                                                    @if($prova->professor == '-')
                                                                        <td></td>
                                                                    @else
                                                                        <td>
                                                                            <center>{{ $prova->getDisciplina->abreviacao }}
                                                                                <br>
                                                                                <small>({{ $prova->professor }})</small>
                                                                            </center>
                                                                        </td>
                                                                    @endif
                                                                @endforeach
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> <!-- /.box-body -->
                                        </div><!-- box -->
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="info-aulas">
            <div class="tab-pane fade active in" id="mural">
                <div class="list-group">
                    <hr>
                    <div class="list-group-item">
                        <div class="row-content">
                            <!-- box -->
                            @foreach($info as $turma)
                                <div class="box">
                                    <div class="box-header with-border panel-group " data-toggle="collapse"
                                         href="#{{ $turma->codigo }}99">
                                        <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool"
                                                    data-widget="collapse" data-toggle="tooltip" title=""
                                                    data-original-title="Collapse">
                                                <i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="panel-collapse collapse " id="{{ $turma->codigo }}99" style="">
                                        <div class="row panel-body">
                                            <div class="col-sm-12">
                                                <table id="example2"
                                                       class="table table-bordered table-hover dataTable"
                                                       role="grid" aria-describedby="example2_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>
                                                            <center>Data</center>
                                                        </th>
                                                        <th>
                                                            1º Horário <br>(7h10 às 8h00)
                                                        </th>
                                                        <th>
                                                            2º Horário <br>(8h00 às 8h50)
                                                        </th>
                                                        <th>
                                                            3º Horário <br>(8h50 às 9h40)
                                                        </th>
                                                        <th>
                                                            4º Horário <br>(10h00 às 10h50)
                                                        </th>
                                                        <th >
                                                            5º Horário <br>(10h50 às 11h40)
                                                        </th>
                                                        <th>
                                                            6º Horário <br>(11h40 às 12h30)
                                                        </th>
                                                        <th>
                                                            7º Horário <br>(14h00 às 14h50)
                                                        </th>
                                                        <th>
                                                            8º Horário <br>(14h50 às 15h40)
                                                        </th>
                                                        <th>
                                                            9º Horário <br>(16h00 às 16h50)
                                                        </th>
                                                        <th>
                                                            10º Horário <br>(16h50 às 17h30)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr role="row">
                                                        <td>Segunda</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Segunda')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Terça</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Terça')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quarta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quarta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quinta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quinta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sexta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sexta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sábado</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sábado')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> <!-- /.box-body -->
                                </div><!-- box -->
                            @endforeach
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="eletro-aulas">
            <div class="tab-pane fade active in" id="mural">
                <div class="list-group">
                    <hr>
                    <div class="list-group-item">
                        <div class="row-content">
                            <!-- box -->
                            @foreach($eletro as $turma)
                                <div class="box">
                                    <div class="box-header with-border panel-group " data-toggle="collapse"
                                         href="#{{ $turma->codigo }}9912">
                                        <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool"
                                                    data-widget="collapse" data-toggle="tooltip" title=""
                                                    data-original-title="Collapse">
                                                <i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="panel-collapse collapse " id="{{ $turma->codigo }}9912" style="">
                                        <div class="row panel-body">
                                            <div class="col-sm-12">
                                                <table id="example2"
                                                       class="table table-bordered table-hover dataTable"
                                                       role="grid" aria-describedby="example2_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>
                                                            <center>Data</center>
                                                        </th>
                                                        <th>
                                                            1º Horário <br>(7h10 às 8h00)
                                                        </th>
                                                        <th>
                                                            2º Horário <br>(8h00 às 8h50)
                                                        </th>
                                                        <th>
                                                            3º Horário <br>(8h50 às 9h40)
                                                        </th>
                                                        <th>
                                                            4º Horário <br>(10h00 às 10h50)
                                                        </th>
                                                        <th >
                                                            5º Horário <br>(10h50 às 11h40)
                                                        </th>
                                                        <th>
                                                            6º Horário <br>(11h40 às 12h30)
                                                        </th>
                                                        <th>
                                                            7º Horário <br>(14h00 às 14h50)
                                                        </th>
                                                        <th>
                                                            8º Horário <br>(14h50 às 15h40)
                                                        </th>
                                                        <th>
                                                            9º Horário <br>(16h00 às 16h50)
                                                        </th>
                                                        <th>
                                                            10º Horário <br>(16h50 às 17h30)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr role="row">
                                                        <td>Segunda</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Segunda')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Terça</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Terça')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quarta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quarta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quinta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quinta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sexta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sexta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sábado</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sábado')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> <!-- /.box-body -->
                                </div><!-- box -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="meca-aulas">
            <div class="tab-pane fade active in" id="mural">
                <div class="list-group">
                    <hr>
                    <div class="list-group-item">
                        <div class="row-content">
                            <!-- box -->
                            @foreach($meca as $turma)
                                <div class="box">
                                    <div class="box-header with-border panel-group " data-toggle="collapse"
                                         href="#{{ $turma->codigo }}992">
                                        <h4 class="box-title"><b>{{ $turma->nomenclatura }}
                                                ({{ ($turma->periodo == 0) ? 'Integral' : 'Noturno' }})</b></h4>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool"
                                                    data-widget="collapse" data-toggle="tooltip" title=""
                                                    data-original-title="Collapse">
                                                <i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="panel-collapse collapse " id="{{ $turma->codigo }}992" style="">
                                        <div class="row panel-body">
                                            <div class="col-sm-12">
                                                <table id="example2"
                                                       class="table table-bordered table-hover dataTable"
                                                       role="grid" aria-describedby="example2_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th>
                                                            <center>Data</center>
                                                        </th>
                                                        <th>
                                                            1º Horário <br>(7h10 às 8h00)
                                                        </th>
                                                        <th>
                                                            2º Horário <br>(8h00 às 8h50)
                                                        </th>
                                                        <th>
                                                            3º Horário <br>(8h50 às 9h40)
                                                        </th>
                                                        <th>
                                                            4º Horário <br>(10h00 às 10h50)
                                                        </th>
                                                        <th >
                                                            5º Horário <br>(10h50 às 11h40)
                                                        </th>
                                                        <th>
                                                            6º Horário <br>(11h40 às 12h30)
                                                        </th>
                                                        <th>
                                                            7º Horário <br>(14h00 às 14h50)
                                                        </th>
                                                        <th>
                                                            8º Horário <br>(14h50 às 15h40)
                                                        </th>
                                                        <th>
                                                            9º Horário <br>(16h00 às 16h50)
                                                        </th>
                                                        <th>
                                                            10º Horário <br>(16h50 às 17h30)
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr role="row">
                                                        <td>Segunda</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Segunda')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Terça</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Terça')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quarta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quarta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Quinta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Quinta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sexta</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sexta')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    <tr role="row">
                                                        <td>Sábado</td>
                                                        @php
                                                            $query = App\Models\Aula::orderBy('horario')
                                                            ->where('turma', '=', $turma->codigo)
                                                            ->where('dia', '=', 'Sábado')
                                                            ->get();
                                                        @endphp
                                                        @foreach($query as $aula )
                                                            @if($aula->professor == '-')
                                                                <td>-</td>
                                                            @else
                                                                <td>
                                                                    <center>{{ $aula->getDisciplina->abreviacao }}<br>
                                                                        <small>({{ $aula->professor }})</small>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> <!-- /.box-body -->
                                </div><!-- box -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

@section('js')
    <
    <script src="{{ asset ('js/mural.js') }}"></script>
@stop
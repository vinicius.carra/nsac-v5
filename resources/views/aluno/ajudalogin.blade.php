@extends('adminlte::page')

@section('title', 'Ajuda - Login')

@section('content_header')
    <h1>Ajuda - Login</h1>
@stop

@section('content')

        <!-- Feature -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Requisitos</h3>
                        </div>
                        <div class="box-body">
                            <ul>
                                <li><small>ter sido matriculado no colégio</small></li>
                                <li><small>ter conhecimento do seu RA ou email cadastrado no sistema</small></li>
                                <li><small>saber a senha</small></li>
                            </ul>
                            <br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Logar</h3>
                        </div>
                        <div class="box-body">
                            <ul>
                                <li><small>No campo "R.A." entre com seu email cadastrado</small></li>
                                <li><small>No campo "Senha" entre com sua senha</small></li>
                            </ul>
                            <br><br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Esqueci meu login ou senha</h3>
                        </div>
                        <div class="box-body">
                            <ul>
                                <li><small>Caso tenha esquecido seu RA ou email cadastrado é necessário entrar em contato com a secretaria para obtenção dos dados</small></li>
                                <li><small>Caso tenha esquecido sua senha use o campo "Esqueci minha senha"</small></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>

@endsection

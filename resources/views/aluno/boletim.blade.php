@extends('adminlte::page')

@section('title', 'Boletim')

@section('content_header')
    <h1>Boletim</h1>
@stop

@section('content')
    <table class="table-striped table-hover container-fluid text-center" id="boletim">
        <thead>
        <tr>
            <th class="col-sm-3"></th>
            <th class="col-sm-2 text-center">1&ordm; B</th>
            <th class="col-sm-2 text-center">2&ordm; B</th>
            <th class="col-sm-2 text-center">3&ordm; B</th>
            <th class="col-sm-2 text-center">4&ordm; B</th>
            <th class="col-sm-1"></th>
        </tr>
        <tr>
            <th style="text-align: left">Matéria</th>
            <th class="row text-center">
                <div class="col-sm-4" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Média da Turma">M.T.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Faltas">F.</div>
            </th>
            <th class="row text-center">
                <div class="col-sm-4" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Média da Turma">M.T.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Faltas">F.</div>
            </th>
            <th class="row text-center">
                <div class="col-sm-4" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Média da Turma">M.T.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Faltas">F.</div>
            </th>
            <th class="row text-center">
                <div class="col-sm-4" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Média da Turma">M.T.</div>
                <div class="col-sm-4" data-toggle="tooltip" title="Faltas">F.</div>
            </th>
            <th class="row text-center">
                <div class="col-md-12" data-toggle="tooltip" title="Média Final">M.F.</div>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($notas as $nota)
            <tr>
                <td style="text-align: left">{{ $nota->disciplina_atribuida->abreviacao }}</td>
                <td class="row">
                    <div class="col-sm-4" {{ $nota->nb(1)=='6.0*' ? 'data-toggle=tooltip title='.$nota->nb1/10 : '' }}>
                        {{ $nota->nb(1) }}
                    </div>
                    <div class="col-sm-4">{{ $nota->disciplina_atribuida->MediaTurma(1) }}</div>
                    <div class="col-sm-4">{{ $nota->fb1 }}</div>
                </td>
                <td class="row">
                    <div class="col-sm-4" {{ $nota->nb(2)=='6.0*' ? 'data-toggle=tooltip title='.$nota->nb2/10 : '' }}>
                        {{ $nota->nb(2) }}
                    </div>
                    <div class="col-sm-4">{{ $nota->disciplina_atribuida->MediaTurma(2) }}</div>
                    <div class="col-sm-4">{{ $nota->fb2 }}</div>
                </td>
                <td class="row">
                    <div class="col-sm-4" {{ $nota->nb(3)=='6.0*' ? 'data-toggle=tooltip title='.$nota->nb3/10 : '' }}>
                        {{ $nota->nb(3) }}
                    </div>
                    <div class="col-sm-4">{{ $nota->disciplina_atribuida->MediaTurma(3) }}</div>
                    <div class="col-sm-4">{{ $nota->fb3 }}</div>
                </td>
                <td class="row">
                    <div class="col-sm-4" {{ $nota->nb(4)=='6.0*' ? 'data-toggle=tooltip title='.$nota->nb4/10 : '' }}>
                        {{ $nota->nb(4) }}
                    </div>
                    <div class="col-sm-4">{{ $nota->disciplina_atribuida->MediaTurma(4) }}</div>
                    <div class="col-sm-4">{{ $nota->fb4 }}</div>
                </td>
                <td>{{ $nota->nf() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('aluno.boletim.PDF') }}" target="_blank" class="col-md-offset-5 col-md-2 btn btn-primary">Gerar PDF</a>
@endsection

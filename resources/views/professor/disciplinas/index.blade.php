@extends('adminlte::page')

@section('title', 'Disciplinas')

@section('content_header')
    <h1>Disciplinas <small>Lista das disciplinas atribuidas</small></h1>
@stop

@section('content')
<div class="box">
    <div class="box-body">

              <table class="table table-striped">
                    <thead>
                         <tr>
                             <th class="text-center">Código</th>
                             <th>Disciplina</th>
                             <th class="text-center">Opções</th>
                         </tr>
                    </thead>

                    <tbody>
                        @if(count($atribuicao)>0)
                            @foreach($atribuicao[0]->disciplinas as $disciplina)
                                <div class="row">
                                    <tr>
                                        <td class="text-center">{{$disciplina->getCodigoCtiAttribute()}}</td>
                                        <td>{{$disciplina->descricao}}</td>
                                        <td class="text-center">
                                            <a href=" {{ Route('professor.disciplinas.show', ['disciplina'=>$disciplina->id]) }}" >
                                                <button type="button" class="btn btn-default btn-sm">
                                                    <span class="glyphicon glyphicon-menu-right"></span>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                </div>
                            @endforeach
                            @endif
                    </tbody>
              </table>
    </div>
</div>

@endsection
@include('professor.notas.aluno')

<div class="box box-primary">
    <div class="box-header with-border"><h3 class="box-title">Lista de Alunos</h3></div>
    <span class="box-body">
        <div class="table-responsive">
            <table class="table table-notas" id="notas">
                <thead>
                <tr>
                    <th colspan="3" title="Dados do aluno(a)">
                        ALUNO(A)
                    </th>
                    <th class="text-center bi1" title="Primeiro bimestre" data-toggle="tooltip" colspan="
                    @if(($dados['bimestre'] == 1)&&(Route::currentRouteName() == 'professor.notas.edit')) 3 @else 2 @endif
                    ">1&ordm; BIMESTRE <br>
                        <small>Média: {{$dados['disciplina']->MediaTurma(1)}}</small>
                    </th>
                    <th class="text-center bi2" title="Segundo bimestre" data-toggle="tooltip" colspan="
                    @if(($dados['bimestre'] == 2)&&(Route::currentRouteName() == 'professor.notas.edit')) 3 @else 2 @endif
                    ">2&ordm; BIMESTRE <br>
                        <small>Média: {{$dados['disciplina']->MediaTurma(2)}}</small>
                    </th>
                    <th class="rec1"></th>
                    <th class="text-center bi3" title="Terceiro bimestre" data-toggle="tooltip" colspan="
                    @if(($dados['bimestre'] == 3)&&(Route::currentRouteName() == 'professor.notas.edit')) 3 @else 2 @endif
                    ">3&ordm; BIMESTRE <br>
                        <small>Média: {{$dados['disciplina']->MediaTurma(3)}}</small>
                    </th>
                    <th class="text-center bi4" title="Quarto bimestre" data-toggle="tooltip" colspan="
                    @if(($dados['bimestre'] == 4)&&(Route::currentRouteName() == 'professor.notas.edit')) 3 @else 2 @endif
                    ">4&ordm; BIMESTRE <br>
                        <small>Média: {{$dados['disciplina']->MediaTurma(4)}}</small>
                    </th>
                    <th class="rec2"></th>
                    <th></th>
                </tr>

                <tr class="cabecalho">

                    <th><span class="text-center" data-toggle="tooltip" title="Número">Nº</span></th>
                    <th><span class="hidden-xs text-center" data-toggle="tooltip"
                              title="Registro Acadêmico">R.A.</span></th>
                    <th><span class="text-left" data-toggle="tooltip" title="Nome do aluno(a)">Nome</span></th>

                    <th class="text-center bi1"><div data-toggle="tooltip" title="Média Bimestral">M.B.</div></th>
                    <th class="text-center bi1"><div data-toggle="tooltip" title="Faltas">F.</div></th>
                    @if(($dados['bimestre'] == 1)&&(Route::currentRouteName() == 'professor.notas.edit'))
                        <th class="text-center bi1"><div data-toggle="tooltip" title="Não compareceu à avaliação bimestral">N.C.</div></th>
                    @endif

                    <th class="text-center bi2"><div data-toggle="tooltip" title="Média Bimestral">M.B.</div></th>
                    <th class="text-center bi2"><div data-toggle="tooltip" title="Faltas">F.</div></th>

                    @if(($dados['bimestre'] == 2)&&(Route::currentRouteName() == 'professor.notas.edit'))
                        <th class="text-center bi2"><div data-toggle="tooltip" title="Não compareceu à avaliação bimestral">N.C.</div></th>
                    @endif

                    <th class="row text-center rec1"><div data-toggle="tooltip" title="Recuperação 1º Semestre">REC.1</div></th>

                    <th class="text-center bi3"><div data-toggle="tooltip" title="Média Bimestral">M.B.</div></th>
                    <th class="text-center bi3"><div data-toggle="tooltip" title="Faltas">F.</div></th>
                    @if(($dados['bimestre'] == 3)&&(Route::currentRouteName() == 'professor.notas.edit'))
                        <th class="text-center bi3"><div data-toggle="tooltip" title="Não compareceu à avaliação bimestral">N.C.</div></th>
                    @endif

                    <th class="text-center bi4"><div data-toggle="tooltip" title="Média Bimestral">M.B.</div></th>
                    <th class="text-center bi4"><div data-toggle="tooltip" title="Faltas">F.</div></th>
                    @if(($dados['bimestre'] == 4)&&(Route::currentRouteName() == 'professor.notas.edit'))
                        <th class="text-center bi4"><div data-toggle="tooltip" title="Não compareceu à avaliação bimestral">N.C.</div></th>
                    @endif

                    <th class="row text-center rec2"><div data-toggle="tooltip" title="Recuperação 2º Semestre">REC.2</div></th>

                    <th class="row text-center"><div data-toggle="tooltip" title="Média Final">M.F.</div></th>

                </tr>
                </thead>
                <tbody>
                @foreach($dados['alunos_matriculados'] as $aluno)
                    <tr>

                        {{--@include('professor.notas.aluno', ['nome' => $aluno->dado->nome, 'matricula' => $aluno->dado->matricula])--}}

                        <td class="row text-center" style="vertical-align: middle;">{{$aluno->ordem}}</td>
                        <td class="text-center" style="vertical-align: middle;">{{$aluno->dado->matricula}}</td>
                        <td class="text-left" style="vertical-align: middle;" data-toggle="modal" data-target="#aluno{{$aluno->dado->matricula}}">
                           {{$aluno->dado->nome}}
                        </td>


                        @php $dispensa = $aluno->dado->dispensado($dados['disciplina']->id) @endphp

                        @isset($dispensa)
                        @php $colspan = 10; @endphp
                        @if(Route::currentRouteName() == 'professor.notas.edit')
                            @php $colspan = 11; @endphp
                        @endif
                        <td class="text-center status-diferente" colspan="{{$colspan}}"
                            style="vertical-align: middle;">Aluno Dispensado</td>
                        <td class="text-center"
                            style=" vertical-align: middle;"> {{ $dispensa->nota }}</td>
                        @endisset

                        @empty($dispensa)

                            @include('professor.notas.notaindividual', ['bimestre' => 1, 'nota' => 'nb1', 'falta' => 'fb1', 'nc' => 'nc1'])
                            @include('professor.notas.notaindividual', ['bimestre' => 2, 'nota' => 'nb2', 'falta' => 'fb2', 'nc' => 'nc2'])
                            @include('professor.notas.notaindividual', ['bimestre' => 5, 'nota' => 'rec1', 'falta' => '', 'nc' => ''])
                            @include('professor.notas.notaindividual', ['bimestre' => 3, 'nota' => 'nb3', 'falta' => 'fb3', 'nc' => 'nc3'])
                            @include('professor.notas.notaindividual', ['bimestre' => 4, 'nota' => 'nb4', 'falta' => 'fb4', 'nc' => 'nc4'])
                            @include('professor.notas.notaindividual', ['bimestre' => 6, 'nota' => 'rec2', 'falta' => '', 'nc' => ''])

                            <td class="text-center medfin" style="vertical-align: middle;">
                            {{Sistema::FormatarNota($aluno->dado->notadisciplina($dados['disciplina']->id)->notafinal)}}
                        </td>
                            @endempty

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </span>
</div>


<!--ALUNOS EM DP-->
@if(count($dados['alunos_dependentes'])>0)
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Alunos em dependência</h3></div>
        <span class="box-body">
            <div class="table-responsive">
                <table class="table" id="dependencia">
                <thead>
                <tr>
                    <th title="Ordem" data-toggle="tooltip" class="text-center">
                        Nº
                    </th>
                    <th title="Registro Acadêmico" data-toggle="tooltip" class="text-center">
                        R.A.
                    </th>
                    <th title="Nome do aluno(a)" data-toggle="tooltip">
                        Nome
                    </th>
                    <th title="Endereço eletrônico" data-toggle="tooltip" class="text-left">
                        E-mail
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($dados['alunos_dependentes'] as $dp)
                        <tr>
                            <td class="text-center">
                                {{$loop->index + 1}}
                            </td>
                            <td class="text-center">
                                {{$dp->matricula}}
                            </td>
                            <td class="text-left">
                                {{$dp->nome}}
                            </td>
                            <td class="text-left">
                                {{$dp->email}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </span>
    </div>
@endif

<!-- ALUNOS ADAPTAÇÃO-->
@if(count($dados['alunos_adaptacao'])>0)
    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Alunos em adaptação</h3></div>
        <span class="box-body">
            <div class="table-responsive">
                <table class="table" id="adaptacao">
                <thead>
                <tr>
                    <th title="Ordem" data-toggle="tooltip" class="text-center">
                        Nº
                    </th>
                    <th title="Registro Acadêmico" data-toggle="tooltip" class="text-center">
                        R.A.
                    </th>
                    <th title="Nome do aluno(a)" data-toggle="tooltip" class="text-left">
                        Nome
                    </th>
                    <th title="Endereço eletrônico" data-toggle="tooltip" class="text-left">
                        E-mail
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($dados['alunos_adaptacao'] as $ad)
                        <tr>
                            <td class="text-center">
                                {{$loop->index + 1}}
                            </td>
                            <td class="text-center">
                                {{$ad->matricula}}
                            </td>
                            <td class="text-left">
                                {{$ad->nome}}
                            </td>
                            <td class="text-left">
                                {{$ad->email}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>

        </span>
    </div>
@endif






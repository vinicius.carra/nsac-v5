var isDisabled = true;

function toggleButtons() {
    $('#salvar').toggle();
    $('#cancelar').toggle();
    $('#alterar').toggle();
}

function toggleForm() {
    if(isDisabled) {
        $('#rua').prop("disabled", false);
        $('#numero').prop("disabled", false);
        $('#complemento').prop("disabled", false);
        $('#cidade').prop("disabled", false);
        $('#bairro').prop("disabled", false);
        isDisabled = false;
    }
    else {
        $('#rua').prop("disabled", true);
        $('#numero').prop("disabled", true);
        $('#complemento').prop("disabled", true);
        $('#cidade').prop("disabled", true);
        $('#bairro').prop("disabled", true);
        isDisabled = true;
    }
}

function excluirTelefone(id) {
    $('#' + id).submit();
}

$(document).ready(function() {
    $('#field_tel').mask('(00) 0000-00000');
    //Run once
    $('#salvar').hide();
    $('#cancelar').hide();

    //Alterar click
    $('#btn-alterar').click(function(e) {
        toggleButtons();
        toggleForm();
    });
    //Salvar click
    $('#btn-salvar').click(function(e) {
        $('#endereco-form').submit();
    });
    $('#endereco-form').submit(function(e) {
        if($('#rua').val() === "")
            e.prevent();
        if($('#numero').val() === "")
            e.prevent();
        if($('#cidade').val() === "")
            e.prevent();
        if($('#bairro').val() === "")
            e.prevent();
    });
    //Cancelar click
    $('#btn-cancelar').click(function(e) {
        location.reload();                
    });
});
<?php

    namespace App\Http\Controllers\Responsavel;

    use App\Http\Controllers\Controller;
    use App\Models\Cidade;
    use App\Models\Dado;
    use App\Models\ResponsavelTelefone;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\File;

    class ResponsavelController extends Controller {
        public function perfil() {
            $perfil = Auth::user()->responsavel;
            $alunos = $perfil->alunos;
            return view('responsavel.perfil')
                ->with('perfil', $perfil)
                ->with('alunos', $alunos);
        }

        public function alunos($matricula) {
            $perfil = Dado::find($matricula);
//            dd($perfil);

            if (File::exists('/imgs/' . $perfil->matricula . '/avatar.jpg')) {
                $avatar = '/imgs/' . $perfil->matricula . '/avatar.jpg';
            } else {
                $avatar = '/imgs/default/avatar.jpg';
            }
            $cidades = Cidade::get()->where('estado', '=', $perfil->getEndereco->getCidade->estado);
            // dd(Auth::user()->dado->telefones);
            return view('responsavel.aluno')
                ->with('perfil', $perfil)
                ->with('cidades', $cidades)
                ->with('avatar', $avatar);
        }

        public function excluirTelefone($id) {
            $telefone = ResponsavelTelefone::find($id);
            $telefone->delete();
            return redirect()->back();
        }

        public function adicionarTelefone(Request $request) {
            $responsavelTelefone = new ResponsavelTelefone;
            $responsavelTelefone->id_responsavel = Auth::user()->responsavel->codigo;
            $responsavelTelefone->telefone = $request->telefone;
            $responsavelTelefone->save();
            return redirect()->back();
        }
    }

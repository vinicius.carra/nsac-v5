<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Turma;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }
	
	 public function mural() {

        $eletro = Turma::get()->where('curso', '=', 0)->where('ano', '=', 2013);
        $info = Turma::get()->where('curso', '=', 1)->where('ano', '=', 2013);
        $meca = Turma::get()->where('curso', '=', 2)->where('ano', '=', 2013);

//         dd($info);

        return view('admin.muralAdmin', ['eletro' => $eletro, 'info' => $info, 'meca' => $meca] );
    }

    public function muralSelecionar(Request $request) {
        $turma = Turma::where('codigo', '=', $request->turma)->first();
        return view('admin.muralAdmin')->with('sala', $turma);
    }
}

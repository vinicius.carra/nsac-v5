<?php

namespace App\Http\Controllers\Aluno;

use App\Declaracao;
use App\Models\CodigoDeclaracao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Cidade;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class DeclaracaoController extends Controller
{
    public function declaracao()
    {
        return view('declaracao');
    }

    public function verificar(Request $request) {
        $declaracao = CodigoDeclaracao::where('codigo', '=', $request->codigo)->first();
        return view('declaracao')->with('declaracao', $declaracao);
        //return redirect()->back()->with('declaracao', $declaracao);
    }

    public function verificarPDF(Request $request) {
        $i_aluno = $request->i_aluno;
        $codigo = $request->codigo;
        $datas = $request->datas;
        $status= $request->status;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('declaracaoVerificaPDF', ['i_aluno' => $i_aluno, 'codigo' => $codigo, 'datas' => $datas, 'status' => $status]);
        return $pdf->stream('declaracaoVerificaPDF.pdf');
    }

}

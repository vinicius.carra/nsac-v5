<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Cidade;
use Illuminate\Support\Facades\Auth;

class AjudaController extends Controller
{
    public function ajuda()
    {
        return view('aluno.ajuda');
    }

    public function ajudalogin(){
        return view('aluno.ajudalogin');
    }

    public function ajudaicone(){
        return view('aluno.ajudaicone');
    }

    public function ajudasenha(){
        return view('aluno.ajudasenha');
    }
}

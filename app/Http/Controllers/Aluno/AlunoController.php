<?php

namespace App\Http\Controllers\Aluno;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cidade;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Models\Prova;
use App\Models\Professor;
use App\Models\Telefone;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Application;
use App\Models\Turma;
use App\Models\Aula;


class AlunoController extends Controller
{

    public function perfil()
    {
        $perfil = Auth::user()->dado;

        if (File::exists('/imgs/' . $perfil->matricula . '/avatar.jpg')) {
            $avatar = '/imgs/' . $perfil->matricula . '/avatar.jpg';
        } else {
            $avatar = '/imgs/default/avatar.jpg';
        }
        $cidades = Cidade::get()->where('estado', '=', $perfil->getEndereco->getCidade->estado);

        return view('aluno.perfil')
            ->with('perfil', $perfil)
            ->with('cidades', $cidades)
            ->with('avatar', $avatar);
    }

    public function excluirTel(Request $request)
    {
        $telefone = Telefone::find($request->codigo);
        $telefone->delete();
        return redirect()->back();
    }

    public function adicionarTel(Request $request)
    {
        $matricula = Auth::user()->dado->matricula;
        $tel = new Telefone;
        $tel->descricao = '';
        $tel->numero = $request->num_telefone;
        $tel->dono = $matricula;
        $tel->save();
        return redirect()->back();
    }


    public function edit(Request $request)
    {
        $perfil = Auth::user()->dado;

        $perfil->getEndereco->logradouro = $request->rua;
        $perfil->getEndereco->numero = $request->numero;

        if ($request->complemento == "" || $request->complemento == "Não disponível")
            $perfil->getEndereco->complemento = "";
        else
            $perfil->getEndereco->complemento = $request->complemento;

        $perfil->getEndereco->cidade = $request->cidade;
        $perfil->getEndereco->bairro = $request->bairro;
        $perfil->getEndereco->timestamps = false;
        $perfil->getEndereco->save();

        return redirect()->back();
    }


    public function mural()
    {

        $eletro = Turma::get()->where('curso', '=', 0)->where('ano', '=', 2013);
        $info = Turma::get()->where('curso', '=', 1)->where('ano', '=', 2013);
        $meca = Turma::get()->where('curso', '=', 2)->where('ano', '=', 2013);

        return view('aluno.mural')
            ->with('eletro', $eletro)
            ->with('info', $info)
            ->with('meca', $meca);
    }

    public function boletim()
    {
        $notas = Auth::user()->dado->notas;
        return view('aluno.boletim', ['notas' => $notas]);
    }

    public function boletimPDF()
    {
        $notas = Auth::user()->dado->notas;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('aluno.boletimtable', ['notas' => $notas]);
        return $pdf->stream('boletim.pdf');
    }

    public function mes() {
        $mes = date('n');
        switch ($mes) {
            case 1:
                return 'Janeiro';
            case 2:
                return 'Fevereiro';
            case 3:
                return 'Março';
            case 4:
                return 'Abril';
            case 5:
                return 'Maio';
            case 6:
                return 'Junho';
            case 7:
                return 'Julho';
            case 8:
                return 'Agosto';
            case 9:
                return 'Setembro';
            case 10:
                return 'Outubro';
            case 11:
                return 'Novembro';
            case 12:
                return 'Dezembro';
            default:
                return 'Indefinido';
        }
    }

    public function declaracaoPDF()
    {
        $perfil = Auth::user()->dado;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('aluno.declaracaoPDF', ['perfil' => $perfil, 'mes' => $this->mes()]);
        return $pdf->stream('declaracao.pdf');
    }

    public function corpodocente()
    {
        $professores = Professor::orderBy('nome')->get();
        return view('aluno.corpodocente', ['professores' => $professores]);
    }

    public function declaracao()
    {
        return view('aluno.declaracao');
    }



}

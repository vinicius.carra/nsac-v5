<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 06/07/2017
 * Time: 10:37
 */

namespace App\Helpers;

use App\Models\Bimestre;
use App\Models\Permissoes;
use App\Models\Disciplina;
use App\Models\Turma;
use App\Models\Professor;
use Carbon\Carbon;
use Illuminate\Support\HtmlString;

class Sistema
{

    public static function PermitidoDigitar($disciplina, $tipo) {
        return Permissoes::where('data_inicio', '<=', date('Y-m-d'))->where('data_fim','>=', date('Y-m-d'))->where('tipo', $tipo)->count() > 0;
    }

    public static function BimestreAtual()
    {
        //$bi = Bimestre::where('data_inicio', '<=', date('Y-m-d'))->where('data_fim','>=', date('Y-m-d'))->first();
        $bi = Bimestre::BimestreAtual()->first();
        if ($bi == null) { $bi = 0; }
        return $bi;
    }

    public static function CodigoCTI($disciplina) {
        $d = Disciplina::find($disciplina);
        if ($d->count() == 0) { return ''; }
        $t = $d->turmas->nomenclatura;
        return substr($t,0,2).$d->codigo.substr($t,2,1);
    }

    public static function FormatarNota($nota) {
        return number_format($nota/10,1,'.','');
    }

    public static function FormatarData($data)
    {
        return substr($data,-2,2)."/".substr($data,-5,2)."/".substr($data,0,4);
    }

    public static function BladeBotaoSidebarDireita($tipo = 'botao', $classe = 'btn btn-default', $texto = 'Abrir Menu Direito', $icon = 'fa fa-edit') {
        if($tipo == 'botao') {
            return new HtmlString('<button class="'.$classe.'" data-toggle="control-sidebar"><i class="'.$icon.'"> </i> '.$texto.'</button>');
        }
        if ($tipo == 'span') {
            return new HtmlString('<span class="'.$classe.'" data-toggle="control-sidebar">'.$texto.'</span>');
        }

    }

    public static function turmastecnico()
    {
        return Turma::where('ano', Carbon::now()->year )
            ->wherehas('cursos', function ($query) {
                $query->where('tipo', 0);
            })
            ->orderBy('nomenclatura')
            ->get();
    }

    public static function turmastecnico_teste()
    {
        return Turma::where('ano', '2015' )
            ->wherehas('cursos', function ($query) {
                $query->where('tipo', 0);
            })
            ->orderBy('nomenclatura')
            ->get();
    }
	
	public static function nome_professor($disciplina,$turma)
	{
		$ano_turma=Turma::find($turma)->ano;
		$atribuicoes=Disciplina::find($disciplina)->atribuicoes->where('data_inicial','like',$ano_turma)->where('data_final','like',$ano_turma);
        foreach ($atribuicoes as $key=>$value)
        {
            $professores[$key]=Professor::find($value->professor);
        }
		return $professores;
	}

}
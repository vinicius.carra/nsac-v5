<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Responsavel extends Model {
        protected $table = "responsaveis";
        protected $connection = "public";

        public function telefones() {
            return $this->hasMany(ResponsavelTelefone::class, 'id_responsavel', 'codigo');
        }

        public function alunos() {
            return $this->hasMany(Dado::class, 'matricula', 'matricula');
        }
    }

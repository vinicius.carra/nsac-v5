<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $connection = "alunos";
    protected $table = "telefone";
    protected $primaryKey = "codigo";
    public $timestamps = false;
}
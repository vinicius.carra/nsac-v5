<?php

namespace App\Models;

use App\Models\Functions\DadoFunctions;
use App\Models\Relations\DadoRelations;
use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;
use App\Models\CodigoDeclaracao;

class Dado extends Model
{

    use DadoRelations;
    use DadoFunctions;

    protected $connection = "alunos";

    protected $table='alunos.dados';

    protected $primaryKey='matricula';

    /**
     * @return string
     */
    public function getsexoFAttribute() {
        return $this->sexo == 0 ? 'o' : 'a';
    }

    public function getConselhoAttribute() {

    }

    /**
     * @return false|string
     */
    public function getIdadeFormatadaAttribute()
    {
        $idade=date('Y')-date('Y',strtotime($this->data_de_nascimento));
        if(date('m')<date('m',strtotime($this->data_de_nascimento)))
            $idade--;
        if(date('m')==date('m',strtotime($this->data_de_nascimento))&&date('d')<date('d',strtotime($this->data_de_nascimento)))
            $idade--;
        return $idade;
    }

    public function generateCodigoDeclaracao() {
        $declaracao = new CodigoDeclaracao;
        $date = new \DateTime('NOW', new \DateTimeZone('America/Sao_Paulo'));
        $codigo = $this->matricula.'-'.number_format(microtime(true), 2,'-', '');

        $declaracao->matricula = $this->matricula;
        $declaracao->codigo = $codigo;
        $declaracao->data_emissao = $date->format('Y-m-d h:m:s');

        $date->add(new \DateInterval('P1M'));
        $declaracao->data_expira = $date->format('Y-m-d h:m:s');

        $declaracao->save();

        return $declaracao;
    }

    public function getSerie() {
        $matriculas = $this->matriculas;
        $matriculaAtual = $matriculas[count($matriculas) - 1];
        return $matriculaAtual->getTurma->ano_curso;
    }


}

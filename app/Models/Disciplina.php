<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Disciplina extends Model {
        public $timestamps = false;
        protected $table = "disciplinas";
        protected $primaryKey = "id";
        protected $connection = 'public';

        public function getCodigoCtiAttribute() {
            $t = $this->turmas->nomenclatura;
            return substr($t, 0, 2) . $this->codigo . substr($t, 2, 1);
        }

        public function alunos_matriculados() {
            return Matricula::where('turma', $this->turma)
                ->with('dado')
                ->orderBy('ordem')
                ->get();
        }

        //RETORNA ALUNOS CURSANDO DEPENDÊNCIA NESTA DISCIPLINA
        public function alunos_dependentes() {
            return Dado::whereHas('disciplinas_dependencia', function ($query) {
                $query->where('disciplina', $this->id);
            })
                ->orderBy('nome')
                ->get();
        }

        //RETORNA ALUNOS CURSANDO ADAPTAÇÃO NESTA DISCIPLINA
        public function alunos_adaptacao() {
            return Dado::whereHas('disciplinas_adaptacao', function ($query) {
                $query->where('disciplina', $this->id);
            })
                ->orderBy('nome')
                ->get();
        }

        //RETORNA ALUNOS DISPENSADOS NESTA DISCIPLINA
        public function alunos_dispensados() {
            return Dado::whereHas('disciplinas_dispensadas', function ($query) {
                $query->where('disciplina', $this->id);
            })
                ->orderBy('nome')
                ->get();
//
//        return Disciplina_Dispensada::where('disciplina',$this->id)
//            ->with('dado')
//            ->get()
//            ->sortBy('dado.nome');
        }

        public function MediaTurma($bimestre) {
            return Round(Nota::where('disciplina', $this->id)->avg('nb' . $bimestre)) / 10;
        }

        public function MediaTurmaGeral() {
            return Round(Nota::where('disciplina', $this->id)->avg('nb1', 'nb2', 'nb3', 'nb4')) / 10;
        }

        //FUNÇÕES DE RELACIONAMENTO:
        public function atribuicoes() {
            return $this->belongsToMany(Atribuicao::class, 'disciplinas_atribuicao', 'disciplina', 'atribuicao')->withPivot('atribuicao');
        }

        public function notas() {
            return $this->hasMany(Nota::class, 'disciplina', 'id');
        }

        public function turmas() {
            return $this->belongsTo(Turma::class, 'turma', 'codigo');
        }

        public function disciplinas_dispensadas() {
            return $this->hasMany(Disciplina_Dispensada::class, 'disciplina', 'id');
        }

        public function disciplinas_dependencia() {
            return $this->hasMany(Disciplina_Dependencia::class, 'disciplina', 'id');
        }

        public function disciplinas_adaptacao() {
            return $this->hasMany(Disciplina_Adaptacao::class, 'disciplina', 'id');
        }

    }

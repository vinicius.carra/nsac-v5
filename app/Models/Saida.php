<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Saida extends Model
{
    protected $connection = "alunos";
    protected $table = "saidas";
}

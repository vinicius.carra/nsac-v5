<?php

namespace App\Models\Relations;


use App\Models\CodigoDeclaracao;
use App\Models\Disciplina_Adaptacao;
use App\Models\Disciplina_Dependencia;
use App\Models\Disciplina_Dispensada;
use App\Models\FichaAvaliacaoAluno;
use App\Models\Matricula;
use App\Models\Nota;
use App\Models\Endereco;
use App\Models\Responsavel;
use App\Models\Telefone;
use App\Models\Atraso;
use App\Models\Saida;

trait DadoRelations
{

    public function notas()
    {
        return $this->hasMany(Nota::class,'aluno','matricula');
    }

    public function matriculas()
    {
        return $this->hasMany(Matricula::class, 'aluno', 'matricula');
    }

    public function disciplinas_dependencia()
    {
        return $this->hasMany(Disciplina_Dependencia::class, 'aluno', 'matricula');
    }

    public function disciplinas_adaptacao()
    {
        return $this->hasMany(Disciplina_Adaptacao::class,'aluno', 'matricula');
    }

    public function disciplinas_dispensadas()
    {
        return $this->hasMany(Disciplina_Dispensada::class,'aluno', 'matricula');
    }

    public function fichaavaliacaoaluno()
    {
        return $this->hasMany(FichaAvaliacaoAluno::class,'matricula','matricula');
    }

    public function getEndereco() {
        return $this->hasOne(Endereco::class, 'codigo', 'endereco');
    }

    public function responsaveis() {
        return $this->hasMany(Responsavel::class, 'matricula', 'matricula');
    }
    
    public function telefones() {
        return $this->hasMany(Telefone::class, 'dono', 'matricula');
    }

    public function codigoDeclaracoes() {
        return $this->hasMany(CodigoDeclaracao::class, 'matricula', matricula);
    }

    public function atrasos(){
        return $this->hasMany(Atraso::class, 'aluno', 'matricula');
    }

    public function saidas() {
        return $this->hasMany(Saida::class, 'aluno', 'matricula');
    }
}
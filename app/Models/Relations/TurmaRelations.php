<?php

namespace App\Models\Relations;

use App\Models\Disciplina;
use App\Models\Matricula;
use App\Models\Curso;

trait TurmaRelations
{

    //FUNÇÕES DE RELACIONAMENTO
    public function disciplinas()
    {
        return $this->hasMany(Disciplina::class,'turma','codigo');
    }

    public function alunos()
    {
        return $this->hasMany(Matricula::class,'turma','codigo');
    }

    public function cursos() {
        return $this->belongsTo(Curso::class, 'curso', 'codigo');
    }

}
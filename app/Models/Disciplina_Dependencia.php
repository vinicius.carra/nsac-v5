<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplina_Dependencia extends Model
{
    //
    protected $table='disciplinas_dependencia';
    protected $primaryKey='id';

    public function dados()
    {
        return $this->hasOne(Dado::class, 'matricula', 'aluno');
    }

    public function disciplinas()
    {
        return $this->hasOne(Disciplina::class, 'id', 'disciplina');
    }


}

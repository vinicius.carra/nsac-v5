<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atraso extends Model
{
    protected $connection = "alunos";
    protected $table = "atrasos";
}

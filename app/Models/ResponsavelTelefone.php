<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsavelTelefone extends Model
{
    protected $table = "responsaveis_telefones";
    protected $connection = "public";
    public $timestamps = false;

    public function responsavel() {
        return $this->belongsTo(Responsavel::class, 'codigo', 'id_responsavel');
    }
}

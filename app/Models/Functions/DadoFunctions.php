<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 16:03
 */

namespace App\Models\Functions;


trait DadoFunctions
{
    public function notadisciplina($disciplina) {
        return $this->notas()->where('notas.disciplina', $disciplina)->first();
    }

    public function possuiFichaAvaliacao($disciplina, $bimestre)
    {
        return $this->fichaavaliacaoaluno()->where('disciplina',$disciplina)->where('bim',$bimestre)->get();
    }

    public function notaabaixode($disciplina, $nota, $bimestre) {
        if ($bimestre == 1)
            return $this->notadisciplina($disciplina)->nb1 < $nota ? true : false;
        elseif ($bimestre == 2)
            return $this->notadisciplina($disciplina)->nb2 < $nota ? true : false;
        elseif ($bimestre == 3)
            return $this->notadisciplina($disciplina)->nb3 < $nota ? true : false;
        elseif ($bimestre == 4)
            return $this->notadisciplina($disciplina)->nb4 < $nota ? true : false;
        elseif ($bimestre == 5)
            return $this->notadisciplina($disciplina)->notafinal < $nota ? true : false;
    }

    public function estaderecuperacao($disciplina, $recuperacao)
    {
        if ($recuperacao == 1) {
            return ($this->notaabaixode($disciplina,6, 1)== true)||($this->notaabaixode($disciplina,6, 2)== true) ? true : false;
        } elseif ($recuperacao == 2) {
            return ($this->notaabaixode($disciplina,6, 3)== true)||($this->notaabaixode($disciplina,6, 4)== true) ? true : false;
        } elseif ($recuperacao == 3) {
            return $this->notaabaixode($disciplina, 6, 5);
        }
    }

    public function dispensado($disciplina) {
        return $this->disciplinas_dispensadas()->where('disciplinas_dispensadas.disciplina',$disciplina)->first();
    }
}
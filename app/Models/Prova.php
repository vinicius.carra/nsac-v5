<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Disciplina;
class Prova extends Model
{
    protected $table = "provas";
    protected $primaryKey = "id";

    public function cursos()
    {
        return $this->belongsToMany(Curso::class,'codigo','curso');
    }

    public function getDisciplina()
    {
        return $this->belongsTo(Disciplina::class,'disciplina','codigo');
    }

    public function getTurma()
    {
        return $this->hasOne(Turma::class,'codigo','turma');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodigoDeclaracao extends Model
{
    protected $table = "codigo_declaracao";
    public $timestamps = false;

    public function dado() {
        return $this->hasOne(Dado::class, 'matricula', 'matricula');
    }

}

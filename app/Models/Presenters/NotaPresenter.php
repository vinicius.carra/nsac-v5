<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 15:42
 */

namespace App\Models\Presenters;

trait NotaPresenter
{

    public function getnb1FAttribute()
    {
        if ($this->rec1 == 1) {
            return 6;
        } else {
            return $this->nb1/10;
        }
    }

    public function getnb2FAttribute()
    {
        if ($this->rec1 == 1) {
            return 6;
        } else {
            return $this->nb2/10;
        }
    }

    public function getnb3FAttribute()
    {
        if  ($this->rec2 == 1) {
            return 6;
        } else {
            return $this->nb3/10;
        }
    }

    public function getnb4FAttribute()
    {
        if ($this->rec2 == 1) {
            return 6;
        } else {
            return $this->nb4/10;
        }
    }

    public function getFaltaTotalAttribute()
    {
        return $this->fb1 + $this->fb2 + $this->fb3 + $this->fb4;
    }

    public function getnotapreparcialAttribute()
    {
        //LEVA RECUPERACAO SEMESTRAL EM CONTA
        $soma = $this->nb1F+$this->nb2F+$this->nb3F+$this->nb4F;
        $notafinal = $soma/4;
        return (Float) number_format($notafinal, 1, '.', '');
    }

    public function getnotaprefinalAttribute()
    {
        if ($this->recfinal == 1) {
            return 6;
        } else {
            $notafinal = $this->notapreparcial;
            if ($notafinal >= 6) {
                return (Float)number_format($notafinal, 1, '.', '');
            } elseif ($notafinal >= 3) {
                if (($this->conselhoparcial == true) || $this->recurso1 == true) {
                    return 6;
                } else {
                    return (Float)number_format($notafinal, 1, '.', '');
                }
            } else {
                if (($this->conselhoparcial == true) || $this->recurso1 == true) {
                    return 3;
                } else {
                    return (Float)number_format($notafinal, 1, '.', '');
                }
            }
        }
    }


    //COMPLETAR
    public function getnotaposfinalAttribute()
    {
        $nota = $this->notaprefinal;
        if ($nota > 6) {
            return $nota;
        } elseif($nota > 3) {
            if (($this->conselhofinal == true)||($this->recurso2 == true)||($this->recurso3 == true)) {
                return 6;
            }
        }
    }

}
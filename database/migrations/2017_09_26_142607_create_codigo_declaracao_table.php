<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigoDeclaracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigo_declaracao', function (Blueprint $table) {
            $table->increments('id');
            $table->char('matricula', 7);
            $table->string('codigo', 50);
            $table->dateTime('data_emissao');
            $table->dateTime('data_expira');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codigo_declaracao');
    }
}

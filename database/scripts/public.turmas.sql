--
-- TOC entry 2755 (class 0 OID 23513)
-- Dependencies: 272
-- Data for Name: turmas; Type: TABLE DATA; Schema: public; Owner: adde
--

INSERT INTO turmas VALUES (1, 2013, 1, 0, 174603209, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (2, 2013, 1, 0, 174603126, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (3, 2013, 2, 0, 174603217, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (4, 2013, 2, 0, 174603134, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (5, 2013, 3, 0, 174603225, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (6, 2013, 3, 0, 174603142, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (7, 2013, 1, 1, 174603233, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (8, 2013, 1, 1, 174603241, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (10, 2013, 2, 1, 174603258, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (9, 2013, 1, 1, 174603159, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (12, 2013, 2, 1, 174603167, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (13, 2013, 3, 1, 174603274, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (14, 2013, 3, 1, 174603282, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (15, 2013, 3, 1, 174603175, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (16, 2013, 1, 2, 174603084, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (17, 2013, 1, 2, 174603092, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (18, 2013, 2, 2, 174603183, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (19, 2013, 2, 2, 174603100, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (20, 2013, 3, 2, 174603191, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (21, 2013, 3, 2, 174603118, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (22, 2013, 1, 3, 174603290, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (23, 2013, 1, 3, 174603324, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (24, 2013, 1, 3, 174603316, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (25, 2013, 1, 3, 174603332, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (26, 2013, 2, 3, 174603340, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (27, 2013, 2, 3, 174603308, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (28, 2013, 2, 3, 174603357, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (29, 2013, 2, 3, 174603365, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (30, 2013, 3, 3, 174603373, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (31, 2013, 3, 3, 174603381, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (32, 2013, 3, 3, 174603399, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (33, 2013, 3, 3, 174603407, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (68, 2011, 1, 0, NULL, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (69, 2011, 1, 0, NULL, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (70, 2011, 2, 0, NULL, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (71, 2011, 2, 0, NULL, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (72, 2011, 3, 0, NULL, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (73, 2011, 3, 0, NULL, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (74, 2011, 1, 1, NULL, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (75, 2011, 1, 1, NULL, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (76, 2011, 2, 1, NULL, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (77, 2011, 1, 1, NULL, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (78, 2011, 2, 1, NULL, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (79, 2011, 2, 1, NULL, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (80, 2011, 3, 1, NULL, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (81, 2011, 3, 1, NULL, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (82, 2011, 3, 1, NULL, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (83, 2011, 1, 2, NULL, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (84, 2011, 1, 2, NULL, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (85, 2011, 2, 2, NULL, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (86, 2011, 2, 2, NULL, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (87, 2011, 3, 2, NULL, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (88, 2011, 3, 2, NULL, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (89, 2011, 1, 3, NULL, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (90, 2011, 1, 3, NULL, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (91, 2011, 1, 3, NULL, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (92, 2011, 1, 3, NULL, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (93, 2011, 2, 3, NULL, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (94, 2011, 2, 3, NULL, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (95, 2011, 2, 3, NULL, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (96, 2011, 2, 3, NULL, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (97, 2011, 3, 3, NULL, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (98, 2011, 3, 3, NULL, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (99, 2011, 3, 3, NULL, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (100, 2011, 3, 3, NULL, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (101, 2011, 3, 1, NULL, '73D', 'D', 1, true);
INSERT INTO turmas VALUES (102, 2010, 1, 0, NULL, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (103, 2010, 1, 0, NULL, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (104, 2010, 2, 0, NULL, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (105, 2010, 2, 0, NULL, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (106, 2010, 3, 0, NULL, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (107, 2010, 3, 0, NULL, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (108, 2010, 1, 1, NULL, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (109, 2010, 1, 1, NULL, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (110, 2010, 2, 1, NULL, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (111, 2010, 1, 1, NULL, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (112, 2010, 2, 1, NULL, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (113, 2010, 2, 1, NULL, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (114, 2010, 3, 1, NULL, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (115, 2010, 3, 1, NULL, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (116, 2010, 3, 1, NULL, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (117, 2010, 1, 2, NULL, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (118, 2010, 1, 2, NULL, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (119, 2010, 2, 2, NULL, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (120, 2010, 2, 2, NULL, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (121, 2010, 3, 2, NULL, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (51, 2012, 2, 2, 164193633, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (53, 2012, 3, 2, 164193641, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (36, 2012, 2, 0, 164193666, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (38, 2012, 3, 0, 164193674, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (40, 2012, 1, 1, 164193682, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (41, 2012, 1, 1, 164193690, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (42, 2012, 2, 1, 164193708, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (44, 2012, 2, 1, 164193716, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (46, 2012, 3, 1, 164193724, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (47, 2012, 3, 1, 164193732, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (55, 2012, 1, 3, 164193740, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (56, 2012, 1, 3, 164193757, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (57, 2012, 1, 3, 164193765, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (58, 2012, 1, 3, 164193773, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (59, 2012, 2, 3, 164193781, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (60, 2012, 2, 3, 164193799, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (61, 2012, 2, 3, 164193807, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (62, 2012, 2, 3, 164193815, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (52, 2012, 2, 2, 164193534, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (54, 2012, 3, 2, 164193542, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (35, 2012, 1, 0, 164193559, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (37, 2012, 2, 0, 164193567, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (39, 2012, 3, 0, 164193575, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (43, 2012, 1, 1, 164193583, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (45, 2012, 2, 1, 164193591, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (48, 2012, 3, 1, 164193609, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (67, 2012, 3, 1, 164193617, '73D', 'D', 1, true);
INSERT INTO turmas VALUES (63, 2012, 3, 3, 164193823, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (64, 2012, 3, 3, 164193831, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (65, 2012, 3, 3, 164193849, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (66, 2012, 3, 3, 164193856, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (122, 2010, 3, 2, NULL, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (123, 2010, 1, 3, NULL, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (124, 2010, 1, 3, NULL, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (125, 2010, 1, 3, NULL, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (126, 2010, 1, 3, NULL, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (127, 2010, 2, 3, NULL, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (128, 2010, 2, 3, NULL, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (129, 2010, 2, 3, NULL, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (130, 2010, 2, 3, NULL, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (131, 2010, 3, 3, NULL, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (132, 2010, 3, 3, NULL, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (133, 2010, 3, 3, NULL, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (134, 2010, 3, 3, NULL, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (135, 2010, 3, 1, NULL, '73D', 'D', 1, true);
INSERT INTO turmas VALUES (11, 2013, 2, 1, 174603266, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (137, 2014, 1, 2, 183559434, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (138, 2014, 1, 2, 183559285, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (139, 2014, 2, 2, 183559442, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (140, 2014, 2, 2, 183559293, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (141, 2014, 3, 2, 183559459, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (142, 2014, 3, 2, 183559301, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (143, 2014, 1, 0, 183559467, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (144, 2014, 1, 0, 183559251, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (145, 2014, 2, 0, 183559475, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (146, 2014, 2, 0, 183559269, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (147, 2014, 3, 0, 183559483, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (148, 2014, 3, 0, 183559277, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (149, 2014, 1, 1, 183559491, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (150, 2014, 1, 1, 183559509, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (151, 2014, 1, 1, 183559210, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (152, 2014, 2, 1, 183559517, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (153, 2014, 2, 1, 183559525, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (154, 2014, 2, 1, 183559236, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (155, 2014, 3, 1, 183559533, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (156, 2014, 3, 1, 183559541, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (157, 2014, 3, 1, 183559244, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (159, 2014, 1, 3, 183559319, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (160, 2014, 1, 3, 183559327, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (161, 2014, 1, 3, 183559335, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (162, 2014, 1, 3, 183559343, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (163, 2014, 2, 3, 183559350, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (164, 2014, 2, 3, 183559368, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (165, 2014, 2, 3, 183559376, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (166, 2014, 2, 3, 183559384, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (167, 2014, 3, 3, 183559392, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (168, 2014, 3, 3, 183559400, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (169, 2014, 3, 3, 183559418, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (170, 2014, 3, 3, 183559426, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (171, 2014, 1, 1, 183559228, '71D', 'D', 1, true);
INSERT INTO turmas VALUES (172, 2015, 1, 2, 195679881, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (173, 2015, 1, 2, 195680004, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (174, 2015, 2, 2, 195679899, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (176, 2015, 3, 2, 195679907, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (178, 2015, 1, 0, 195679915, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (180, 2015, 2, 0, 195679923, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (182, 2015, 3, 0, 195679931, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (184, 2015, 1, 1, 195679949, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (185, 2015, 1, 1, 195679956, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (187, 2015, 2, 1, 195679964, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (188, 2015, 2, 1, 195679972, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (190, 2015, 3, 1, 195679980, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (191, 2015, 3, 1, 195679998, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (193, 2015, 1, 3, 195680111, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (194, 2015, 1, 3, 195680129, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (195, 2015, 1, 3, 195680137, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (196, 2015, 1, 3, 195680145, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (197, 2015, 2, 3, 195680152, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (198, 2015, 2, 3, 195680160, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (199, 2015, 2, 3, 195680178, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (200, 2015, 2, 3, 195680186, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (201, 2015, 3, 3, 195680194, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (202, 2015, 3, 3, 195680202, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (203, 2015, 3, 3, 195680210, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (204, 2015, 3, 3, 195680228, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (175, 2015, 2, 2, 195680012, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (177, 2015, 3, 2, 195680020, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (179, 2015, 1, 0, 195680038, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (181, 2015, 2, 0, 195680046, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (183, 2015, 3, 0, 195680053, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (186, 2015, 1, 1, 195680061, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (205, 2015, 1, 1, 195680079, '71D', 'D', 1, true);
INSERT INTO turmas VALUES (189, 2015, 2, 1, 195680087, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (206, 2015, 2, 1, 195680095, '72D', 'D', 1, true);
INSERT INTO turmas VALUES (192, 2015, 3, 1, 195680103, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (49, 2012, 1, 2, 164193625, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (34, 2012, 1, 0, 164193658, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (50, 2012, 1, 2, 164193526, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (208, 2016, 1, 2, 204448138, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (209, 2016, 2, 2, 204448245, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (210, 2016, 2, 2, 204448146, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (212, 2016, 3, 2, 204448153, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (214, 2016, 1, 0, 204448161, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (216, 2016, 2, 0, 204448179, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (218, 2016, 3, 0, 204448187, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (221, 2016, 1, 1, 204448195, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (222, 2016, 1, 1, 204448203, '71D', 'D', 1, true);
INSERT INTO turmas VALUES (225, 2016, 2, 1, 204448211, '72C', 'C', 1, true);
INSERT INTO turmas VALUES (228, 2016, 3, 1, 204448229, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (207, 2016, 1, 2, 204448237, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (211, 2016, 3, 2, 204448252, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (213, 2016, 1, 0, 204448260, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (215, 2016, 2, 0, 204448278, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (217, 2016, 3, 0, 204448286, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (219, 2016, 1, 1, 204448294, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (220, 2016, 1, 1, 204448302, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (223, 2016, 2, 1, 204448310, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (224, 2016, 2, 1, 204448328, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (226, 2016, 3, 1, 204448336, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (227, 2016, 3, 1, 204448344, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (229, 2016, 1, 3, 204448351, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (230, 2016, 1, 3, 204448369, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (231, 2016, 1, 3, 204448377, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (233, 2016, 1, 3, 204448385, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (234, 2016, 2, 3, 204448393, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (235, 2016, 2, 3, 204448401, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (236, 2016, 2, 3, 204448419, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (237, 2016, 2, 3, 204448427, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (238, 2016, 3, 3, 204448435, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (239, 2016, 3, 3, 204448443, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (240, 2016, 3, 3, 204448450, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (241, 2016, 3, 3, 204448468, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (277, 2017, 2, 1, 215369893, '72D', 'D', 1, true);
INSERT INTO turmas VALUES (265, 2017, 1, 3, 215369562, '81A', 'A', 0, true);
INSERT INTO turmas VALUES (266, 2017, 1, 3, 215369570, '81B', 'B', 0, true);
INSERT INTO turmas VALUES (267, 2017, 1, 3, 215369588, '81C', 'C', 0, true);
INSERT INTO turmas VALUES (268, 2017, 1, 3, 215369596, '81D', 'D', 0, true);
INSERT INTO turmas VALUES (269, 2017, 2, 3, 215369604, '82A', 'A', 0, true);
INSERT INTO turmas VALUES (270, 2017, 2, 3, 215369612, '82B', 'B', 0, true);
INSERT INTO turmas VALUES (271, 2017, 2, 3, 215369620, '82C', 'C', 0, true);
INSERT INTO turmas VALUES (272, 2017, 2, 3, 215369638, '82D', 'D', 0, true);
INSERT INTO turmas VALUES (273, 2017, 3, 3, 215369646, '83A', 'A', 0, true);
INSERT INTO turmas VALUES (274, 2017, 3, 3, 215369653, '83B', 'B', 0, true);
INSERT INTO turmas VALUES (275, 2017, 3, 3, 215369661, '83C', 'C', 0, true);
INSERT INTO turmas VALUES (276, 2017, 3, 3, 215369679, '83D', 'D', 0, true);
INSERT INTO turmas VALUES (243, 2017, 1, 2, 215369687, '11A', 'A', 0, true);
INSERT INTO turmas VALUES (245, 2017, 2, 2, 215369695, '12A', 'A', 0, true);
INSERT INTO turmas VALUES (247, 2017, 3, 2, 215369703, '13A', 'A', 0, true);
INSERT INTO turmas VALUES (249, 2017, 1, 0, 215369711, '51A', 'A', 0, true);
INSERT INTO turmas VALUES (251, 2017, 2, 0, 215369729, '52A', 'A', 0, true);
INSERT INTO turmas VALUES (253, 2017, 3, 0, 215369737, '53A', 'A', 0, true);
INSERT INTO turmas VALUES (255, 2017, 1, 1, 215369745, '71A', 'A', 0, true);
INSERT INTO turmas VALUES (256, 2017, 1, 1, 215369752, '71B', 'B', 0, true);
INSERT INTO turmas VALUES (259, 2017, 2, 1, 215369760, '72A', 'A', 0, true);
INSERT INTO turmas VALUES (260, 2017, 2, 1, 215369778, '72B', 'B', 0, true);
INSERT INTO turmas VALUES (262, 2017, 3, 1, 215369786, '73A', 'A', 0, true);
INSERT INTO turmas VALUES (263, 2017, 3, 1, 215369794, '73B', 'B', 0, true);
INSERT INTO turmas VALUES (244, 2017, 1, 2, 215369802, '11B', 'B', 1, true);
INSERT INTO turmas VALUES (246, 2017, 2, 2, 215369810, '12B', 'B', 1, true);
INSERT INTO turmas VALUES (248, 2017, 3, 2, 215369828, '13B', 'B', 1, true);
INSERT INTO turmas VALUES (250, 2017, 1, 0, 215369836, '51B', 'B', 1, true);
INSERT INTO turmas VALUES (252, 2017, 2, 0, 215369844, '52B', 'B', 1, true);
INSERT INTO turmas VALUES (254, 2017, 3, 0, 215369851, '53B', 'B', 1, true);
INSERT INTO turmas VALUES (264, 2017, 3, 1, 215369869, '73C', 'C', 1, true);
INSERT INTO turmas VALUES (257, 2017, 1, 1, 215369877, '71C', 'C', 1, true);
INSERT INTO turmas VALUES (261, 2017, 2, 1, 215369885, '72C', 'C', 1, true);
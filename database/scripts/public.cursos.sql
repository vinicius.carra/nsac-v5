--
-- TOC entry 2710 (class 0 OID 23363)
-- Dependencies: 225
-- Data for Name: cursos; Type: TABLE DATA; Schema: public; Owner: adde
--

INSERT INTO cursos VALUES ('Técnico de Nível Médio em Eletrônica', 'Eletrônica', 0, 5, 0, 2011, 2017, 'Resolução SE Teste', true);
INSERT INTO cursos VALUES ('Técnico de Nível Médio em Informática', 'Informática', 1, 7, 0, 2011, 2017, 'ATO SE Teste Info', true);
INSERT INTO cursos VALUES ('Técnico de Nível Médio em Mecânica', 'Mecânica', 2, 1, 0, 2011, 2017, 'Deliberação CE Teste MEcanica', true);
INSERT INTO cursos VALUES ('Ensino Médio', 'Ensino Médio', 3, 8, 1, 2011, 2017, 'ATO SE nº 106 de 07/04/1967', true);
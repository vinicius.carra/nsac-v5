--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

-- Started on 2017-08-30 08:10:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2377 (class 0 OID 46840)
-- Dependencies: 207
-- Data for Name: temas; Type: TABLE DATA; Schema: public; Owner: adde
--

INSERT INTO temas (id, nome, l5, l4, l3, l2, l1, c0, d1, d2, d3, d4, d5) VALUES (1, 'Azul', '#F2F9FE', '#D2EAFD', '#A6D4FA', '#79BFF8', '#4DAAF6', '#2196F3', '#0C87EB', '#0B78D1', '#0A69B7', '#085A9D', '#074B83');
INSERT INTO temas (id, nome, l5, l4, l3, l2, l1, c0, d1, d2, d3, d4, d5) VALUES (2, 'Cinza', '#F9F9F9', '#ECECEC', '#D8D8D8', '#C5C5C5', '#B1B1B1', '#9E9E9E', '#8E8E8E', '#7E7E7E', '#6F6F6F', '#5F5F5F', '#4F4F4F');
INSERT INTO temas (id, nome, l5, l4, l3, l2, l1, c0, d1, d2, d3, d4, d5) VALUES (3, 'Roxo', '#FAF0FC', '#EFCEF4', '#DE9EEA', '#CE6DDF', '#BE3DD4', '#9C27B0', '#8C239E', '#7C1F8D', '#6D1B7B', '#5D1769', '#4E1358');
INSERT INTO temas (id, nome, l5, l4, l3, l2, l1, c0, d1, d2, d3, d4, d5) VALUES (4, 'Verde', '#f4faf4', '#dbefdc', '#b7dfb8', '#93cf95', '#6ec071', '#4caf50', '#459c48', '#3d8b40', '#357a38', '#2e6830', '#265728');


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 206
-- Name: temas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: adde
--

SELECT pg_catalog.setval('temas_id_seq', 4, true);


-- Completed on 2017-08-30 08:10:24

--
-- PostgreSQL database dump complete
--

